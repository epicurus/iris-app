import React, { useState, useEffect } from 'react';
import fetch from 'node-fetch';
import calParser from 'cal-parser';
import { format, compareAsc } from 'date-fns';
import i18n from 'i18n-js';

const MAX_SUMMARY_LENGTH = 333;

export type EventDate = {
    startDate: string;
    endDate: string;
    timezone: string;
    title: string;
    summary: string;
    description: string;
};

export const noEvent: EventDate = {
    startDate: format(new Date(), 'yyyy-MM-dd'),
    endDate: format(new Date(), 'yyyy-MM-dd'),
    timezone: '',
    title: 'No upcoming events right now.',
    summary: 'Please check later...',
    description: ''
};

export type CalendarOptions = {
    startDate?: Date;
};

type EventDatesResponse = EventDate[] | Error | null;

type IcalEventParams = {
    tzid: string;
};

type IcalEventDate = {
    value: string;
    params: IcalEventParams;
};

type IcalEventInfo = {
    value: string;
};

type IcalEvent = {
    dtstart: IcalEventDate;
    dtend: IcalEventDate;
    dtstamp: string;
    uid: IcalEventInfo;
    'recurrence-id': IcalEventDate;
    created: string;
    description: IcalEventInfo;
    'last-modified': string;
    location: IcalEventInfo;
    sequence: IcalEventInfo;
    status: IcalEventInfo;
    summary: IcalEventInfo;
    transp: IcalEventInfo;
};

type I18nDescription = { [key: string]: string };

const getI18nDescription: (description: string) => string = (description) => {
    const descriptions: string[] = description.split('///');
    const localeRe = /^\s*([a-z]{2}\-[A-Z]+)\s*(<br>)*/;
    const i18nDescriptions: I18nDescription = {};
    descriptions.forEach((description: string, i: number) => {
        if (i === 0) {
            i18nDescriptions.en = description;
        } else {
            const match = description.match(localeRe);

            if (match) {
                const [fullMatch, locale] = match;
                i18nDescriptions[locale] = description.substr(fullMatch.length);
            }
        }
    });

    return i18nDescriptions[i18n.locale] || i18nDescriptions.en;
};

const parseEventDates = (events: IcalEvent[]): EventDate[] => {
    const summaryRe = /^([\w\W]+)---([\w\W]*)$/;
    return events
        .map(
            (event: IcalEvent): EventDate => {
                const startDate = event.dtstart.value.toString();
                const endDate = event.dtend.value.toString();
                const timezone = event.dtstart.params?.tzid;
                const title = event.summary.value;
                const description = getI18nDescription(event.description.value);
                const summaryMatch = description.match(summaryRe);
                let summary = summaryMatch ? summaryMatch[1] : description;
                if (summary.length > MAX_SUMMARY_LENGTH) {
                    summary = `${summary.substr(0, MAX_SUMMARY_LENGTH)}...`;
                }

                return {
                    startDate,
                    endDate,
                    timezone,
                    title,
                    summary,
                    description
                };
            }
        )
        .sort((a: EventDate, b: EventDate) =>
            compareAsc(new Date(a.startDate), new Date(b.startDate))
        );
};

const useGoogleCalendar = (
    icalUrl: string,
    options: CalendarOptions = {}
): EventDatesResponse | null => {
    const startDate = options.startDate || new Date();

    const [eventsResponse, setEventsResponse] = useState<EventDatesResponse>(null);

    useEffect(() => {
        async function loadCalendar() {
            try {
                const res = await fetch(icalUrl);

                if (res.ok) {
                    const icalText = await res.text();
                    const { events } = calParser.parseString(icalText);

                    if (events.length) {
                        const parsedEvents = parseEventDates(events);
                        // const filteredEvents = parsedEvents.filter((event: EventDate) => {
                        //     return compareAsc(new Date(event.startDate), startDate) === 1;
                        // });
                        const filteredEvents = parsedEvents;

                        setEventsResponse(filteredEvents);
                    } else {
                        setEventsResponse([noEvent]);
                    }
                } else {
                    console.error(res.status);
                }
            } catch (error) {
                setEventsResponse(error);
            }
        }

        loadCalendar();
    }, [icalUrl]);

    return eventsResponse;
};

export default useGoogleCalendar;
