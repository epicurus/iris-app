module.exports = {
    env: {
        es6: true,
        jest: true
    },
    extends: [
        'plugin:@typescript-eslint/recommended', // Uses the recommended rules from @typescript-eslint/eslint-plugin
        'eslint:recommended',
        'plugin:@typescript-eslint/eslint-recommended',
        'plugin:@typescript-eslint/recommended',
        // 'prettier'
        'plugin:prettier/recommended' // Enables eslint-plugin-prettier and eslint-config-prettier. This will display prettier errors as ESLint errors. Make sure this is always the last configuration in the extends array.
    ],
    parser: '@typescript-eslint/parser',
    parserOptions: {
        ecmaVersion: 2020,
        sourceType: 'module',
        ecmaFeatures: {
            jsx: true // Allows for the parsing of JSX
        }
    },
    plugins: ['jest', '@typescript-eslint', 'prettier'],
    rules: {
        semi: ['error', 'always'],
        indent: ['error', 4],
        'no-console': 1, // Means warning
        'prettier/prettier': 2 // Means error
    },
    settings: {
        react: {
            version: 'detect' // Tells eslint-plugin-react to automatically detect the version of React to use
        }
    },
    overrides: [
        {
            files: ['*.stories.tsx'],
            rules: {
                'import/no-extraneous-dependencies': 'off'
            }
        }
    ]
};
