import React from 'react';
import { Text, useWindowDimensions, ViewStyle, TextStyle } from 'react-native';
import { Card } from 'react-native-elements';
import AppLoading from 'expo-app-loading';
import { useFonts } from 'expo-font';
import _ from 'lodash';

import useStyles from '../../hooks/useStyles';
import { EpikuretDate } from '../../types';
import AudioPanel from '../AudioPanel';
import Thought from '../Thought';

type Props = {
    epikurets?: EpikuretDate[];
    details?: boolean;
    promotion?: 'home';
};

export default function Epikuret({ epikurets, details = false }: Props): JSX.Element {
    const epikuret = epikurets && epikurets[0];
    if (!epikuret) return <Text>Epikuret not found.</Text>;

    const { title, subtitle, audio, thought } = epikuret || {
        title: 'Epikuret not found ...'
    };

    const styles = useStyles(commonStyles);
    const width = useWindowDimensions().width - 10;

    const [fontsLoaded] = useFonts({
        TheGirlNextDoor_Regular: require('../../assets/fonts/TheGirlNextDoor-Regular.ttf'),
        PTSans_Regular: require('../../assets/fonts/PTSans-Regular.ttf')
    });

    if (!fontsLoaded) {
        return <AppLoading />;
    }

    const audioPanel = details && audio ? <AudioPanel audio={audio || ''} /> : null;

    if (details) {
        const titleStyle = {
            ...styles.epikuretThoughtsTitle,
            fontSize: 28,
            lineHeight: 36
        };
        return (
            <Card containerStyle={{ width, ...styles.epikuretThoughtsCard }}>
                <Card.Title style={titleStyle}>{title}</Card.Title>
                <Card.FeaturedSubtitle style={styles.epikuretThoughtsSubtitle}>
                    {subtitle}
                </Card.FeaturedSubtitle>
                {audioPanel}
                <Thought thought={thought || []} details={details} />
            </Card>
        );
    } else {
        const titleStyle = {
            ...styles.epikuretThoughtsTitle,
            fontSize: 22,
            lineHeight: 28
        };
        return (
            <Card containerStyle={{ width, ...styles.epikuretThoughtsCard }}>
                <Card.Title style={titleStyle}>{title}</Card.Title>
                <Card.FeaturedSubtitle style={styles.epikuretThoughtsSubtitle}>
                    {subtitle}
                </Card.FeaturedSubtitle>
            </Card>
        );
    }
}

const commonStyles: { [key: string]: ViewStyle & TextStyle } = {
    epikuretThoughtsCard: {
        marginTop: 15,
        width: '95%'
    },
    epikuretThoughtsTitle: {
        fontWeight: 'normal',
        fontFamily: 'PTSans_Regular',
        textAlign: 'center',
        paddingTop: 20,
        fontSize: 22,
        lineHeight: 28
    },
    epikuretThoughtsSubtitle: {
        fontWeight: 'normal',
        fontFamily: 'PTSans_Regular',
        textAlign: 'center',
        fontSize: 16
    }
};
