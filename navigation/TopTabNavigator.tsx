import * as React from 'react';
import { Ionicons } from '@expo/vector-icons';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import { createStackNavigator } from '@react-navigation/stack';
import { Image } from 'react-native';
import { get } from 'lodash';

import irisIcon from '../assets/images/iris.jpg';

import Colors from '../constants/Colors';
import useColorScheme from '../hooks/useColorScheme';
import TabHomeScreen from '../screens/tabs/tab-home-screen';
import TabIrisScreen from '../screens/tabs/tab-iris-screen';
import TabTextsScreen from '../screens/tabs/tab-texts-screen';
import TabTalentsScreen from '../screens/tabs/tab-talents-screen';
import TabEventsScreen from '../screens/tabs/tab-event-screen';
import {
    TopTabParamList,
    TabHomeParamList,
    TabIrisParamList,
    TabTextsParamList,
    TabTalentsParamList,
    TabEventsParamList
} from '../types';
import useContent from '../hooks/useContent';

const Tab = createMaterialTopTabNavigator<TopTabParamList>();

export default function TopTabNavigator(): JSX.Element {
    const { ui } = useContent();

    const colorScheme = useColorScheme();
    return (
        <Tab.Navigator
            initialRouteName="Home"
            tabBarOptions={{
                activeTintColor: Colors[colorScheme].tint,
                showIcon: true,
                style: {
                    marginTop: 38
                },
                tabStyle: { padding: 0 },
                indicatorStyle: { backgroundColor: Colors[colorScheme].tint }
            }}
        >
            <Tab.Screen
                name={get(ui, 'navigation.tabs.home')}
                component={TabHomeNavigator}
                options={{
                    tabBarIcon: ({ color }) => <TabBarIcon name="home" color={color} />
                }}
            />
            <Tab.Screen
                name={get(ui, 'navigation.tabs.iris')}
                component={TabIrisNavigator}
                options={{
                    tabBarIcon: () => (
                        <Image
                            source={irisIcon}
                            style={{ width: 28, height: 28, borderRadius: 12 }}
                        />
                    )
                }}
            />
            <Tab.Screen
                name={get(ui, 'navigation.tabs.texts')}
                component={TabTextsNavigator}
                options={{
                    tabBarIcon: ({ color }) => <TabBarIcon name="pencil" color={color} />
                }}
            />
            <Tab.Screen
                name={get(ui, 'navigation.tabs.talents')}
                component={TabTalentsNavigator}
                options={{
                    tabBarIcon: ({ color }) => <TabBarIcon name="people" color={color} />
                }}
            />
            <Tab.Screen
                name={get(ui, 'navigation.tabs.events')}
                component={TabEventsNavigator}
                options={{
                    tabBarIcon: ({ color }) => <TabBarIcon name="calendar-outline" color={color} />
                }}
            />
        </Tab.Navigator>
    );
}

// You can explore the built-in icon families and icons on the web at:
// https://icons.expo.fyi/
function TabBarIcon(props: { name: React.ComponentProps<typeof Ionicons>['name']; color: string }) {
    return <Ionicons size={24} style={{ marginBottom: -3 }} {...props} />;
}

// Each tab has its own navigation stack, you can read more about this pattern here:
// https://reactnavigation.org/docs/tab-based-navigation#a-stack-navigator-for-each-tab
const TabHomeStack = createStackNavigator<TabHomeParamList>();

function TabHomeNavigator() {
    return (
        <TabHomeStack.Navigator headerMode="none">
            <TabHomeStack.Screen
                name="TabHomeScreen"
                component={TabHomeScreen}
                options={{ headerTitle: 'Home Screen' }}
            />
        </TabHomeStack.Navigator>
    );
}

const TabIrisStack = createStackNavigator<TabIrisParamList>();

function TabIrisNavigator() {
    return (
        <TabIrisStack.Navigator headerMode="none">
            <TabIrisStack.Screen
                name="TabIrisScreen"
                component={TabIrisScreen}
                options={{ headerTitle: 'Iris Screen' }}
            />
        </TabIrisStack.Navigator>
    );
}

const TabTextsStack = createStackNavigator<TabTextsParamList>();

function TabTextsNavigator() {
    return (
        <TabTextsStack.Navigator headerMode="none">
            <TabTextsStack.Screen
                name="TabTextsScreen"
                component={TabTextsScreen}
                options={{ headerTitle: 'Texts Screen' }}
            />
        </TabTextsStack.Navigator>
    );
}

const TabTalentsStack = createStackNavigator<TabTalentsParamList>();

function TabTalentsNavigator() {
    return (
        <TabTalentsStack.Navigator headerMode="none">
            <TabTalentsStack.Screen
                name="TabTalentsScreen"
                component={TabTalentsScreen}
                options={{ headerTitle: 'Talents Screen' }}
            />
        </TabTalentsStack.Navigator>
    );
}

const TabEventsStack = createStackNavigator<TabEventsParamList>();

function TabEventsNavigator() {
    return (
        <TabEventsStack.Navigator headerMode="none">
            <TabEventsStack.Screen
                name="TabEventsScreen"
                component={TabEventsScreen}
                options={{ headerTitle: 'Events Screen' }}
            />
        </TabEventsStack.Navigator>
    );
}
