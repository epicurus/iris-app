import color from 'color';

const tintColorLight = '#941574';
const tintColorDark = '#eb9d96';
const tintEpicurusColorLight = '#1c0968';
const tintEpicurusColorDark = '#aab4f3';
const tintThoughtColorLight = '#1c0968';
const tintThoughtColorDark = '#bb80c7';
const tintBookColorLight = '#1c0968';
const tintBookColorDark = '#9de1ac';
const tintGlossaryColorLight = '#1c0968';
const tintGlossaryColorDark = '#7da7f9';
const buttonGroupColorLight = '#4c4c4c';
const buttonGroupColorDark = '#afafaf';

export type AppColors = {
    text: string;
    background: string;
    canvas: string;
    tint: string;
    tintEpicurus: string;
    tintThought: string;
    tintBook: string;
    tintGlossary: string;
    tabIconDefault: string;
    tabIconSelected: string;
    buttonGroupSelection: string;
    buttonGroupSeparator: string;
};

type Colors = {
    light: AppColors;
    dark: AppColors;
};

const colors: Colors = {
    light: {
        text: color(tintColorLight).darken(0.25).hex(),
        background: '#fff',
        canvas: '#f2e2e2',
        tint: tintColorLight,
        tintEpicurus: tintEpicurusColorLight,
        tintThought: tintThoughtColorLight,
        tintBook: tintBookColorLight,
        tintGlossary: tintGlossaryColorLight,
        tabIconDefault: '#ccc',
        tabIconSelected: tintColorLight,
        buttonGroupSelection: buttonGroupColorLight,
        buttonGroupSeparator: color(buttonGroupColorLight).lighten(0.25).hex()
    },
    dark: {
        text: color(tintColorDark).lighten(0.25).hex(),
        background: '#000',
        canvas: '#1e1414',
        tint: tintColorDark,
        tintEpicurus: tintEpicurusColorDark,
        tintThought: tintThoughtColorDark,
        tintBook: tintBookColorDark,
        tintGlossary: tintGlossaryColorDark,
        tabIconDefault: '#ccc',
        tabIconSelected: tintColorDark,
        buttonGroupSelection: buttonGroupColorDark,
        buttonGroupSeparator: color(buttonGroupColorDark).darken(0.25).hex()
    }
};

export default colors;
