import React from 'react';
import { ScrollView, FlexStyle, TextStyle, StatusBar, Pressable } from 'react-native';
import { StackScreenProps } from '@react-navigation/stack';
import _ from 'lodash';

import Epikuret from '../components/Epikuret';
import useStyles from '../hooks/useStyles';
import { RootStackParamList, TopTabParamList } from '../types';

type Props = StackScreenProps<RootStackParamList & TopTabParamList, 'Epikuret'>;

export default function EpikuretScreen({ navigation, route }: Props): JSX.Element {
    const { epikurets } = route.params;
    const styles = useStyles(commonStyles);

    return (
        <ScrollView style={styles.container}>
            <Pressable onPress={() => navigation.goBack()}>
                <Epikuret epikurets={epikurets} details />
            </Pressable>
        </ScrollView>
    );
}

const commonStyles: { [key: string]: FlexStyle & TextStyle } = {
    container: {
        paddingTop: StatusBar.currentHeight
    }
};
