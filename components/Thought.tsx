import React from 'react';
import useStyles from '../hooks/useStyles';
import { Text, ViewStyle, TextStyle } from 'react-native';

type Props = {
    thought: string[];
    details: boolean;
};

const Thought = ({ thought }: Props): JSX.Element => {
    const styles = useStyles(commonStyles);

    return (
        <>
            {thought.map((line, i) => {
                return (
                    <Text key={i} style={styles.thought}>
                        {line}
                    </Text>
                );
            })}
        </>
    );
};

const commonStyles: { [key: string]: ViewStyle & TextStyle } = {
    thought: {
        fontFamily: 'PTSans_Regular',
        fontSize: 20,
        lineHeight: 26,
        textAlign: 'justify',
        paddingTop: 10,
        paddingBottom: 10
    }
};

export default Thought;
