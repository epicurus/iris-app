import React from 'react';
import { Text, View, useWindowDimensions, ViewStyle, TextStyle, Linking } from 'react-native';
import { Card, ButtonGroup } from 'react-native-elements';
import AppLoading from 'expo-app-loading';
import { useFonts } from 'expo-font';
import _ from 'lodash';

import useStyles from '../../hooks/useStyles';
import useContent from '../../hooks/useContent';
import { EpikuretDate } from '../../types';
import Thought from '../Thought';

const BUTTON_SIZE = 32;

type Props = {
    epikurets?: EpikuretDate[];
    details?: boolean;
    promotion?: 'home';
};

export default function Epikuret({ epikurets, details = false }: Props): JSX.Element {
    const epikuret = epikurets && epikurets[0];
    if (!epikuret) return <Text>Epikuret not found.</Text>;

    const { ui } = useContent();

    const { title, subtitle, cover, thought, buy, download } = epikuret || {
        title: 'Epikuret not found ...'
    };

    const styles = useStyles(commonStyles);
    const width = useWindowDimensions().width - 10;

    const [fontsLoaded] = useFonts({
        PTSans_Regular: require('../../assets/fonts/PTSans-Regular.ttf')
    });

    if (!fontsLoaded) {
        return <AppLoading />;
    }

    if (details) {
        const titleStyle = {
            ...styles.epikuretBooksTitle,
            fontSize: 24,
            lineHeight: 30
        };
        const buyTypes = ui.screens.texts.buyTypes as unknown as { [key: string]: string };

        const onPressBuy = (type: string, index: number) => {
            const url = type === 'download' ? download : buy ? buy[type][index].url : '';
            Linking.canOpenURL(url).then((supported) => {
                if (supported) {
                    Linking.openURL(url);
                } else {
                    console.log("Don't know how to open URI: " + url);
                }
            });
        };

        const buys = (buy && Object.keys(buy)) || [];
        if (download) {
            buys.push('download');
        }
        let buyLinks: JSX.Element | null = null;
        if (buys && buys.length > 0) {
            buyLinks = (
                <>
                    {buys.map((type) => {
                        const buttons =
                            type === 'download'
                                ? [ui.screens.texts.download]
                                : (buy &&
                                      buy[type] &&
                                      buy[type].map(({ provider }): string => provider)) ||
                                  [];

                        return (
                            <View key={type}>
                                <Card.FeaturedSubtitle style={styles.epikuretBooksSubtitle}>
                                    {buyTypes[type]}
                                </Card.FeaturedSubtitle>
                                <ButtonGroup
                                    onPress={(index: number) => onPressBuy(type, index)}
                                    buttons={buttons}
                                    containerStyle={{
                                        height: BUTTON_SIZE,
                                        backgroundColor: 'white',
                                        borderWidth: 0,
                                        display: 'flex'
                                    }}
                                    buttonContainerStyle={{
                                        backgroundColor: 'transparent',
                                        borderColor: styles.epikuretBooksCard.backgroundColor
                                    }}
                                    buttonStyle={{
                                        backgroundColor: styles.epikuretBooksSubtitle.color,
                                        borderColor: 'yellow'
                                    }}
                                    selectedButtonStyle={{
                                        backgroundColor: 'transparent'
                                    }}
                                    innerBorderStyle={{
                                        color: 'transparent'
                                    }}
                                    textStyle={{
                                        color: styles.epikuretBooksCard.backgroundColor
                                    }}
                                    selectedTextStyle={{
                                        color: 'transparent'
                                    }}
                                />
                            </View>
                        );
                    })}
                </>
            );
        }

        return (
            <Card containerStyle={{ width, ...styles.epikuretBooksCard }}>
                <Card.Title style={titleStyle}>{title}</Card.Title>
                <Card.FeaturedSubtitle style={styles.epikuretBooksSubtitle}>
                    {subtitle}
                </Card.FeaturedSubtitle>
                <View style={{ display: 'flex', alignItems: 'center' }}>
                    <Card.Image source={cover} style={styles.epikuretBooksImage} />
                </View>

                <Thought thought={thought || []} details={details} />

                {buyLinks}

                <Text>...</Text>
            </Card>
        );
    } else {
        return (
            <Card containerStyle={{ width, ...styles.epikuretBooksCard }}>
                <View style={{ display: 'flex', alignItems: 'center' }}>
                    <Card.Image source={cover} style={styles.epikuretBooksImage} />
                </View>
                <Card.FeaturedSubtitle style={styles.epikuretBooksSubtitle}>
                    {subtitle}
                </Card.FeaturedSubtitle>
            </Card>
        );
    }
}

const commonStyles: { [key: string]: ViewStyle & TextStyle } = {
    epikuretBooksCard: {
        marginTop: 15,
        width: '95%'
    },
    epikuretBooksTitle: {
        fontWeight: 'normal',
        fontFamily: 'PTSans_Regular',
        textAlign: 'center',
        paddingTop: 20
    },
    epikuretBooksSubtitle: {
        paddingTop: 10,
        fontWeight: 'normal',
        fontFamily: 'PTSans_Regular',
        textAlign: 'center',
        fontSize: 18
    },
    epikuretBooksImage: {
        width: 240,
        height: 360
    }
};
