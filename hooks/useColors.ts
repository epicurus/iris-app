import Colors, { AppColors } from '../constants/Colors';

import useColorScheme from './useColorScheme';

export default function useColors(): AppColors {
    const colorScheme = useColorScheme();
    return Colors[colorScheme];
}
