import React, { useCallback } from 'react';
import { StyleProp, TextStyle } from 'react-native';
import { Text } from 'react-native';
import * as Linking from 'expo-linking';

type Props = {
    href: string;
    style?: StyleProp<TextStyle>;
};

const Anchor: React.FunctionComponent<Props> = ({
    href,
    style,
    children
}: React.PropsWithChildren<Props>): JSX.Element => {
    const handlePress = useCallback(() => {
        Linking.openURL(href);
    }, [href]);

    return (
        <Text onPress={() => handlePress()} style={style}>
            {children}
        </Text>
    );
};

export default Anchor;
// <Anchor href="https://google.com">Go to Google</Anchor>
// <Anchor href="mailto:support@expo.io">Email support</Anchor>
