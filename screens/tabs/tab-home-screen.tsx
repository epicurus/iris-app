import React, { useState, useEffect, useMemo } from 'react';
import { FlexStyle, Pressable, ScrollView } from 'react-native';
import { StackScreenProps } from '@react-navigation/stack';
import Constants from 'expo-constants';
import { compareAsc } from 'date-fns';
import _ from 'lodash';
import { EpikuretDate, TopTabParamList, RootStackParamList } from '../../types';

import useStyles from '../../hooks/useStyles';
import useContent from '../../hooks/useContent';
import Epikuret from '../../components/Epikuret';

import useGoogleCalendar, { EventDate, noEvent } from '../../hooks/useGoogleCalendar';
import Event from '../../components/Event';

type Props = StackScreenProps<TopTabParamList & RootStackParamList, 'Home'>;

const googleCalendarIcalURl = Constants.manifest.extra.googleCalendarIcalUrl;

export default function TabHomeScreen({ navigation }: Props): JSX.Element {
    const [displayedEvents, setDisplayedEvents] = useState<EventDate[]>([]);
    const [nextEvent, setNextEvent] = useState<EventDate | null>(null);

    const styles = useStyles(commonStyles);
    const { epikurets } = useContent();
    const events = useGoogleCalendar(googleCalendarIcalURl);

    const matchingEpikurets = useMemo(
        () => epikurets.filter((epikuret: EpikuretDate) => epikuret.promotion === 'home'),
        [epikurets]
    );

    useEffect(() => {
        if (Array.isArray(events)) {
            setDisplayedEvents(events);
        } else if (events instanceof Error) {
            console.error(Error);
        }
    }, [events]);

    useEffect(() => {
        if (displayedEvents) {
            const now = new Date();
            const next = displayedEvents.find(
                (event) => compareAsc(new Date(event.startDate), now) === 1
            );

            if (next) setNextEvent(next);
        }
    }, [displayedEvents]);

    const epikuretElements = matchingEpikurets.map((matchingEpikuret, index) => (
        <Pressable
            key={index}
            onPress={() =>
                navigation.navigate('Epikuret', {
                    epikurets: [matchingEpikuret],
                    details: true
                })
            }
        >
            <Epikuret epikurets={[matchingEpikuret]} details={false} />
        </Pressable>
    ));

    return (
        <ScrollView style={styles.container}>
            <Pressable onPress={() => navigation.navigate('Event', { event: nextEvent })}>
                <Event event={nextEvent || noEvent} />
            </Pressable>
            {epikuretElements}
        </ScrollView>
    );
}

const commonStyles: { [key: string]: FlexStyle } = {
    container: {
        flex: 1
    }
};
