const appName = 'Iris Meeting Point';

export default {
    name: appName,
    slug: 'iris-work',
    version: '1.0.0',
    orientation: 'portrait',
    description:
        'This app is built around the work of Iris Johansson. Here you find events, like the Garden of Epicurus, books, thoughts, recommended videos and books and people, that Iris wants to promote. This app is an open source internet project, meaning you can take part if you like. Just go to https://bitbucket.org/epicurus/iris-app .',
    icon: './assets/images/irisflower.png',
    scheme: 'myapp',
    userInterfaceStyle: 'dark',
    splash: {
        image: './assets/images/splash.png',
        resizeMode: 'contain',
        backgroundColor: '#ffffff'
    },
    updates: { fallbackToCacheTimeout: 0 },
    assetBundlePatterns: ['**/*'],
    ios: { supportsTablet: true },
    android: {
        adaptiveIcon: {
            foregroundImage: './assets/images/adaptive-icon.png',
            backgroundColor: '#FFFFFF'
        }
    },
    web: { favicon: './assets/images/favicon.png' },
    extra: {
        googleCalendarIcalUrl:
            'https://calendar.google.com/calendar/ical/s215etedi7gu0ef0otn6g57sts%40group.calendar.google.com/public/basic.ics'
    }
};
