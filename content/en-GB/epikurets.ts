import epikuretImage0 from '../images/2bbfe9ebef6074125834cb0e4a91aff3b700d94d.jpg';
import epikuretImage1 from '../images/5bb62d6c8fb559471ad6b7f778a3711cf1d517bf.jpg';
import epikuretImage2 from '../images/aadbcafba46e94a4ab184c142644a0540a09acba.jpg';
import epikuretImage3 from '../images/30b27002ea9821fe4a902fa8afbc9084583aacd3.jpg';
import epikuretImage4 from '../images/a36376dd700419546de7e37c49338a15d334b736.jpg';
export default [
  {
    "epikuretId": "02-Reading-the-athmosphere-in-zoom",
    "title": "About the difference between Zoom meetings and physical meetings",
    "type": "epicurus",
    "audio": "https://storage.googleapis.com/ewer/_home_michael_Der-Garten-des-Epikur_storage_Garden-Of-Epicurus_2021-02-23_02-Reading-the-athmosphere-in-zoom_sv-SE.mp3",
    "promotion": "home",
    "subtitle": "Garden of Epicurus, 23. Feb 2021",
    "dialog": [
      "ris, during the introduction you said that you look into the atmosphere from time to time and read what you see there. How does that work via Zoom?",
      "Iris: It's just like when we are one metre apart, or if someone is in Australia and someone is in Sweden, there is no distance in the atmosphere. It's just here and now and present. In the dark of the universe there is no distance, there is no time and space. There is only here and now and presence. All the information from the childhood of humanity to the demise of humanity is there in the atmosphere. There may also be information from other planets, but since I don't speak that language, I don't know anything about it. Yes, I call this seeing, but it is actually a knowing of it. I just call it seeing, so it doesn't seem to be such a mumbo jumbo."
    ]
  },
  {
    "epikuretId": "12-Energy to work",
    "title": "A thousand times more power than we need",
    "type": "epicurus",
    "audio": "https://storage.googleapis.com/ewer/_home_michael_Der-Garten-des-Epikur_storage_Garden-Of-Epicurus_2021-02-23_12-Energy to work_sv-SE.mp3",
    "promotion": "home",
    "subtitle": "Garden of Epicurus, 23. Feb 2021",
    "dialog": [
      "I often encounter the phenomenon that people tell me that in the past people somehow had more energy, that they could work the whole day through, and that today that is somehow less. And I asked myself, is this perhaps connected to the third and fourth communication dimension, that somehow something doesn't fit properly any more, or what could that be?",
      "Iris: It has to do with our imagination and our adaptation to a large extent. For example, when you talk about power or energy, you talk about how if there was a certain amount of it, that is a majority misunderstanding. The moment you get afraid that your energy is not going to be enough, you get afraid and that's when you lack access to power. When you trust that you have a thousand times more power than you need, the energy is in constant movement. As soon as you get afraid that you won't have enough energy, you get afraid and then the power sags and that's the reason why people slacken or have no more power. So when we are in this thinking error, this slackening of strength comes sooner and sooner and we lack access to the strength. That is the reason. If we then do all the necessary uncomfortable things immediately, the body becomes happy and satisfied. It's like when you're cold and you put something on, but you sit or lie as still as possible so that you can save as much strength as possible, so that you can warm yourself, so to speak. Then you don't get warm. But if you start dancing or playing the guitar or singing or running back and forth and then wrap yourself up, the warmth stays with you and you might stay warm all night long. When you have discovered that, then you will see that you can work through it so easily without any problems. And then Iris talks about her own work. She can be active and work for 20 hours at a stretch, but then she says, we're not talking about that now, that's work narcomania, that's too much on the other side and not too little on the one side. One has one's faults and shortcomings. Balance or equilibrium, that's what matters."
    ]
  },
  {
    "epikuretId": "01-indifference",
    "title": "Indifference is the other end of love",
    "type": "epicurus",
    "audio": "https://storage.googleapis.com/ewer/_home_michael_Der-Garten-des-Epikur_storage_Garden-Of-Epicurus_2021-03-23_01-indifference_sv-SE.mp3",
    "promotion": "home",
    "subtitle": "Garden of Epicurus, 23. Mar 2021",
    "dialog": [
      "Yes, I have been dealing a lot with indifference as a phenomenon lately. So, I'm concerned with the fact that I observe that I and other people simply remain cold when it comes to quite important things. Of course, it's easier for me to see that in others, and here I have an example: In your Irrgefühl book you write that most illnesses come from erroneous feelings and then I have often told or mentioned this and always noticed that it passes, even with people who think a lot of you. And I would have liked to hear something about how it is that things, where most illnesses come from, just flow by.",
      "Iris: Love is a state that is there all the time in the atmosphere. And it's quite easy to bring that state up to love by amusing one of my senses, by having fun. The state of love, when you raise it to a feeling, you become open and vulnerable, and all kinds of sensations, feelings arise in you. If love falls over in one way or another, then it becomes hate and that is the other end, the other side of the same bridge, of the same branch. So all these sensations, all these feelings, love to hate, it is the life force that creates all these feelings. On the other hand, the other side of it is indifference. When something stands in the way of this state and blocks this state of love, when love is not allowed to come upon us, so to speak, then indifference arises."
    ]
  },
  {
    "epikuretId": "04-advertizment-and-tiredness",
    "title": "Getting tired because of advertising",
    "type": "epicurus",
    "audio": "https://storage.googleapis.com/ewer/_home_michael_Der-Garten-des-Epikur_storage_Garden-Of-Epicurus_2021-03-23_04-advertizment-and-tiredness_sv-SE.mp3",
    "promotion": "home",
    "subtitle": "Garden of Epicurus, 23. Mar 2021",
    "dialog": [
      "And the other question is that I also often notice that I'm very tired, how should I say it, when I'm walking around in the city and I see advertisements and things and then at some point there comes a kind of tiredness, so oh my God, I think it's too much. And in this buying society that we are, so that I always get a feeling that I should now, I don't know, look different or care more about stylish clothes or about household and I don't buy anything, but just even looking at that makes me very tired. I wonder if she can say something about this tiredness, so why do I get tired when I see a lot of things?",
      "Iris: The thing you react to, the thing that makes you tired, that's when you're out in the city, you see the colours, you see the movements, you hear the sounds and these advertisements are made in such a way that we can't resist them. We have to take them in. The advertisements are cunningly made to go in, and the intention is that through the advertisements, on a conscious level, you either like that or dislike that. You get into an affect through it. The intention is that it should become like a tattoo on the inside. So when you go into a shop and you see this trademark that you've been tattooed with, the intention is that you should be attracted to that trademark and that you should acquire that. It is, in a way, the intention that we should be adapted to that. And with all the adaptation, there is a kind of tiredness, a kind of sadness."
    ]
  },
  {
    "epikuretId": "14-World-situation-changed-since-covid",
    "title": "On the threat to democracy",
    "type": "epicurus",
    "audio": "https://storage.googleapis.com/ewer/_home_michael_Der-Garten-des-Epikur_storage_Garden-Of-Epicurus_2021-03-23_14-World-situation-changed-since-covid_sv-SE.mp3",
    "promotion": "home",
    "subtitle": "Garden of Epicurus, 23. Mar 2021",
    "dialog": [
      "Yes, the question is about the world situation and how Iris thinks we can behave. Since the beginning of the Corona pandemic, I think it has become more and more clear to me that something that is being communicated on a large scale does not correspond more and more with reality. Then on Saturday I went to a demonstration here in Stockum for the first time and I went there in the name of democracy, which even in Sweden has been damaged so easily, quite simply now, by a law where a maximum of eight people are allowed to gather. Yes, I was filmed and after a few hours 40 or 500 people had written on Facebook what a lunatic I am, that I threaten other people's lives because of my existence. And that raises a lot of questions.",
      "Iris: I almost finished editing a booklet on secondary life today. And in this booklet there is a detailed answer to your question. The title is: Talking together ... about secondary life. But I will still give you an answer now, because the question is a very interesting question. When we talk about reality, we mean everything that can be taken as an object, that can be made into a thing. With this one, we have created a zero-sum game. We have created an economisation of it. And in contrast to the primary, this is secondary, elementary and selectable. People can dispose of this, we can direct this. Because this is a game and there are rules, there are winners and losers. If someone wins, there is someone else who loses. And I think that this game is what you suspect. And this game, quite rightly, also endangers democracy, as you have this inkling. It builds on an inhuman thinking and I think it is this that creates a discomfort in you. To understand this, you need to know that this is basically our consciousness as a protection and defence mechanism for survival. And we have been given this prerequisite, because in wild nature we otherwise have few prerequisites for superiority. Our senses are not sharp enough, we don't see enough, we don't hear enough, we don't sense scents accurately enough to cope in wild nature. We need the help of others for quite a long time until we are ready to do something about it and we can do something ourselves. And even when we have grown up, we still run too slowly, or we don't fight well, and we easily fall prey to wild animals. And that's why we got a consciousness and got a speech organ, so that we can exchange with others and we need to be part of a herd. What is it then that characterises the human being. When you are in a group with others in the wild, you make sure that nobody loses out.",
      "Iris: It is what characterises or distinguishes primitive peoples, it is that the people in the group take care of each other in such a way that nobody loses. It is this that creates a sustainable herd for people. Then, when we have organised society in such a way that there are no external dangers, and we use our consciousness to operate our intellect and we make use of our consciousness. We no longer need this consciousness to live in the herd, but instead we make use of our consciousness in an inhuman way. And I think that this is what is squeaking inside you. A system that has been economised is an inhuman system, and it becomes an emotional game. And the worst of the emotional games that take place in our world are the wars. And this game consists in scaring a person to the point of making him inhuman, without that person knowing that he is afraid. That is what radicalisation is. I think that this is what is rubbing inside you, even if you haven't put it into words like that, but you can think about it.",
      "You mean that I am radicalised in a way.",
      "Iris: Not that you are being radicalised, that you are threatened by the radicalisation that is taking place in society. Unfortunately, you are also right that we all get radicalised when we get scared. So, we are not free from it either."
    ]
  },
  {
    "epikuretId": "epicurus-introduction-2021",
    "title": "Epicurus Garden - virtually",
    "type": "thought",
    "origin": "iris",
    "year": "2021",
    "promotion": "none",
    "subtitle": "Iris, 2021",
    "thought": [
      "Epicurus was a philosopher who lived in Greece a few hundred years before our time. He belonged to the upper class, but he had a completely different attitude to life than the one prevailing among other philosophers. He gathered people who were interested in his large park and sat down with them and talked about life and the meaning of life in living as well as finding balance within oneself to live a contented life and what he calls \"wellbeing\". An aledged infamous depiction of which he after his death was accused of was Hedonism, which is an unbridled desire for pleasure and is about optimizing and measuring the intoxication effect of the kicks one could get emotionally, preferably erotically or through excited violence. This was something completely different from Epicurus' balance thinking and the depiction was created to break the epicurean way of relating to things and the balance in conversation and mutuality and the peaceful nourishment in wellbeing that came from this- a form of healthy, simple living.",
      "Epicurus himself had severe rheumatism so every morning students came, carried him out and laid him in the sun. When he then was hot he was laid on a stretcher and pulled out and then coined a winged word, \"even on the stretch bench you can experience an ounce of happiness\". It was because when he was pulled out there were a cogwheel mekanism and every time this let go, the stretch let go a little and this was a moment of relief of pain and of feeling happy. When I read philosophy I found him and he stood for everything I had found outalong the way. So therefore I am a 'weapon baerer' of his. I follow in his footsteps and take care of those who come my way by following his tradition and setting up his garden, right now internationally 'online'. So... if you come to my epicurus' garden, you can bring along the questions you are busy with inside, things you do not think are possible to talk about or get any new input on ... private, personal, philosophical, and so on, that which you yourself have no answer to but would like to have a reflection on. I take all questions seriously and of course I sometimes say I do not know, but often information comes out in the atmosphere, information I can get hold of and convey. That's what philosophizing is and it's so interesting and you are welcome. You can also come just to listen and maybe come up with something to ask about, or not. The side effect of your listening is in the atmosphere that forms itself and enriches in the same way as questions, so ... feel free to come just because you are curious. Sometimes there will also be a bit of primary work to mirror Epicurus' way of working in esteem and respect, relieing and being safe on the inside of oneself and sharing life with others. This philosopher asked big amounts of money from the propertied when they wanted to participate and he let women and slaves join for free and sometimes even a meal and a garment went into the bargain. I like keeping up this tradition by the coordination through others, for the time being through Michael and Benedicte in Germany and coordinator Filip Wall in Sweden."
    ]
  },
  {
    "epikuretId": "thoughts-primary-reference",
    "title": "Primärer Bezugspunkt",
    "type": "thought",
    "origin": "ksk",
    "year": "2013",
    "promotion": "none",
    "subtitle": "Communication as Art, 2013",
    "thought": [
      "REFERENCE POINT, some examples to practice yourself on.",
      "When you are yourself as yourself and you stand in front of the mirror and your image in the mirror is your 'audience' that you are speaking to and you watch, and feel, how you react to what you are saying to yourself.",
      "- Life is the meaning of life and I am living and being here and now in front of this mirror and I feel a little .... and then I look like this.",
      "- The value of life is that I exist and I exist... I can rest in that, in myself and feel ok.. with that... Can't add anything or subtract anything, it's....",
      "- Community I am in... with myself if I am not afraid... and not valuing myself... neither up nor down, just being...",
      "- Security I am in, basic state, when I know my life is not threatened... and when I don't think I have to get something from outside... to be in it. I'm an adult, over 20 years old, and then I'm all grown up... and can connect inside to the fact that I'm capable of taking care of myself... regardless of any outside rescue... is here and now, unchallenged....",
      "- Let go of the idea of doing right... or being good... and turn it around. It's right and you're good when you do nothing... and when you do the everyday necessary without valuing it as heavy or boring... without valuing... If you then discover what is bad or what you think is wrong, focus on that... and then, see what you can learn. Learn, learn, learn and reconcile with the fact that this is reality... Let your contentment come from within as you live this.",
      "- The next step is to correct the errors if there is anything to correct. Some correct themselves just by awareness and not repeating... others cannot be corrected and instead you have to settle for reconciliation... with the fact that it turned out the way it did... Some things can be changed in reality... opinions... corrections... and have understanding, and concretely correct...",
      "- Constantly knowing that things will go wrong... because life requires incompleteness to exist... 'I' am not the root of it, only incomplete and the same goes for 'others', they are not the root of evil, only incomplete brings it about.",
      "- The body produces emotions in order for us to change our beliefs... those that no longer make sense in reality. When we lose someone, we get fed up and cry and grieve until we discharge what the body has produced and then we get a bit sad... Then the idea that we had, that there was something that no longer exists... becomes possible to live without... that we are missing and eventually fall into oblivion. That goes for fear, anger and joy too.",
      "- Intimacy is.... it prevails when we are only in ourselves, in being, here and now.... in ourselves... in the encounter with others... in relation to everything we have around us.... in the togetherness we are part of... etc. Then the body becomes happy and we draw love to feeling... we feel life alive...",
      "- Pain arises in all emotions... pain is the body's natural way of communicating. It raises awareness of the incompleteness of life and gives us space to understand the conditions of life....",
      "- Pain is our ally and we do not die from it... it is at the service of life...",
      "- Autopilot, the protection, escape and defence system that clicks into place when the body thinks it is in mortal danger... does nothing... stop... look around you here and now... see if it's life-threatening... if not, take your time and become aware that you are afraid.",
      "- When there is danger you will automatically follow the impulses coming from the autopilot and they come when they are not relevant too and then you need to stop... become aware of the here and now situation, register the danger, and... rethink...",
      "- SELLING",
      "TO BE ONE WITH OURSELVES, WHAT THAT MEANS IN LIFE.",
      "WELCOME"
    ]
  }
]