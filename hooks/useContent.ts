import content, { LocalesDefault, LocalesAliases } from '../content';
import i18n from 'i18n-js';
import { LanguageContent } from '../types';

export default function useContent(): LanguageContent {
    const locale = LocalesAliases[i18n.locale] || LocalesDefault;

    return content[locale];
}
