import React from 'react';
import { Text, useWindowDimensions, ViewStyle, TextStyle } from 'react-native';
import { Card } from 'react-native-elements';
import HTML from 'react-native-render-html';
import { format } from 'date-fns';
import _ from 'lodash';

import { EventDate, noEvent } from '../hooks/useGoogleCalendar';
import useStyles from '../hooks/useStyles';

type Props = {
    event: EventDate;
    details?: boolean;
};

const buildDateRange = (startDate: Date, endDate: Date) => {
    const hasTime: boolean = startDate.getHours() !== 0;
    const [start, end] = [startDate, endDate].map((date: Date) => ({
        year: date.getFullYear(),
        month: date.getMonth(),
        day: date.getDate(),
        hour: date.getHours(),
        minutes: date.getMinutes()
    }));

    if (start.day === end.day && start.month === end.month && start.year === end.year) {
        return format(startDate, 'd.MMM. y H:mm');
    } else if (start.year === end.year) {
        return `${format(startDate, `d.MMM.${hasTime ? ' H:mm' : ''}`)} - ${format(
            endDate,
            `d.MMM. y${hasTime ? ' H:mm' : ''}`
        )}`;
    } else {
        return `${format(startDate, `d.MMM. y${hasTime ? ' H:mm' : ''}`)} - ${format(
            endDate,
            `d.MMM. y${hasTime ? ' H:mm' : ''}`
        )}`;
    }
};

export default function Event({ event, details = false }: Props) {
    const styles = useStyles(commonStyles);
    const { startDate, endDate, title, summary, description } = event || noEvent;
    const dateRange = buildDateRange(new Date(startDate), new Date(endDate));
    const width = useWindowDimensions().width - 10;
    return (
        <Card containerStyle={{ width, ...styles.card }}>
            <Card.Title style={styles.title}>{title}</Card.Title>
            <Text style={styles.date}>{dateRange}</Text>
            <Card.Divider />
            <HTML
                source={{
                    html: details ? description : summary || '&nbsp'
                }}
                baseFontStyle={styles.html}
            />
            <Card.Divider />
        </Card>
    );
}

const commonStyles: { [key: string]: ViewStyle & TextStyle } = {
    card: {
        marginTop: 20,
        width: '95%'
    },
    title: {
        fontWeight: 'bold',
        fontSize: 16
    },
    date: {
        marginBottom: 10
    }
};
