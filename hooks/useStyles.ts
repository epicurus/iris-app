import color from 'color';
import { StyleSheet, ViewStyle, TextStyle } from 'react-native';
import Colors from '../constants/Colors';
import _ from 'lodash';

import useColorScheme from './useColorScheme';

export default function useStyles(commonStyles: {
    [key: string]: ViewStyle & TextStyle;
}): typeof lightStyles {
    const colorScheme = useColorScheme();
    const merged = _.merge({}, colorScheme === 'light' ? lightStyles : darkStyles, commonStyles);
    return StyleSheet.create(merged);
}

const lightStyles = {
    container: {
        backgroundColor: Colors.light.canvas
    },
    card: {
        borderWidth: 0,
        borderRadius: 10,
        backgroundColor: Colors.light.background
    },
    epikuretEpicurusCard: {
        borderWidth: 0,
        borderRadius: 10,
        backgroundColor: Colors.light.background
    },
    epikuretEpicurusTitle: {
        color: Colors.light.tintEpicurus
    },
    epikuretEpicurusSubtitle: {
        color: color(Colors.light.tintEpicurus).darken(0.25).hex()
    },
    epikuretThoughtsCard: {
        borderWidth: 0,
        borderRadius: 10,
        backgroundColor: Colors.light.background
    },
    epikuretThoughtsTitle: {
        color: Colors.light.tintThought
    },
    epikuretThoughtsSubtitle: {
        color: color(Colors.light.tintThought).darken(0.25).hex()
    },
    epikuretBooksCard: {
        borderWidth: 0,
        borderRadius: 10,
        backgroundColor: Colors.light.background
    },
    epikuretBooksTitle: {
        color: Colors.light.tintBook
    },
    epikuretBooksSubtitle: {
        color: color(Colors.light.tintBook).darken(0.25).hex()
    },
    epikuretBooksImage: {},
    epikuretGlossaryCard: {
        borderWidth: 0,
        borderRadius: 10,
        backgroundColor: Colors.light.background
    },
    epikuretGlossaryTitle: {
        color: Colors.light.tintGlossary
    },
    epikuretGlossarySubtitle: {
        color: color(Colors.light.tintGlossary).darken(0.25).hex()
    },
    epikuretGlossaryVideo: {
        color: color(Colors.light.tintGlossary).darken(0.25).hex()
    },
    epikuretGlossaryVideoButton: {
        color: color(Colors.light.tintGlossary).darken(0.25).hex()
    },
    title: {
        color: Colors.light.tint
    },
    date: {
        color: Colors.light.text
    },
    dialog1: {
        color: Colors.light.text
    },
    dialog2: {
        color: Colors.light.text
    },
    thought: {
        color: Colors.light.text
    },
    html: {
        color: Colors.light.text
    },
    buttonGroupButton: {
        backgroundColor: 'transparent'
    },
    buttonGroupSelectedButton: {
        backgroundColor: Colors.light.buttonGroupSelection
    },
    buttonGroupSelectedText: {
        color: 'white'
    },
    buttonGroupText: {
        color: Colors.light.buttonGroupSelection
    },
    buttonGroupBorder: {
        color: Colors.light.buttonGroupSeparator
    },
    searchBar: {}
};

const darkStyles = {
    container: {
        backgroundColor: Colors.dark.canvas
    },
    card: {
        borderWidth: 1,
        borderColor: '#6b4845',
        borderRadius: 10,
        backgroundColor: Colors.dark.background
    },
    epikuretEpicurusCard: {
        borderWidth: 1,
        borderColor: Colors.dark.tintEpicurus,
        borderRadius: 10,
        backgroundColor: Colors.dark.background
    },
    epikuretEpicurusTitle: {
        color: Colors.dark.tintEpicurus
    },
    epikuretEpicurusSubtitle: {
        color: color(Colors.dark.tintEpicurus).lighten(0.15).hex()
    },
    epikuretGlossaryVideo: {
        color: color(Colors.dark.tintGlossary).lighten(0.25).hex()
    },
    epikuretGlossaryVideoButton: {
        color: color(Colors.dark.tintGlossary).lighten(0.25).hex()
    },
    epikuretThoughtsCard: {
        borderWidth: 1,
        borderColor: Colors.dark.tintThought,
        borderRadius: 10,
        backgroundColor: Colors.dark.background
    },
    epikuretThoughtsTitle: {
        color: Colors.dark.tintThought
    },
    epikuretThoughtsSubtitle: {
        color: color(Colors.dark.tintThought).lighten(0.15).hex()
    },
    epikuretBooksCard: {
        borderWidth: 1,
        borderColor: '#666',
        borderRadius: 10,
        backgroundColor: Colors.dark.background
    },
    epikuretBooksTitle: {
        color: Colors.dark.tintBook
    },
    epikuretBooksSubtitle: {
        color: color(Colors.dark.tintBook).lighten(0.15).hex()
    },
    epikuretBooksImage: {},
    epikuretGlossaryCard: {
        borderWidth: 1,
        borderColor: Colors.dark.tintGlossary,
        borderRadius: 10,
        backgroundColor: Colors.dark.background
    },
    epikuretGlossaryTitle: {
        color: Colors.dark.tintGlossary
    },
    epikuretGlossarySubtitle: {
        color: color(Colors.dark.tintGlossary).lighten(0.15).hex()
    },
    epikuretAudioCard: {
        borderWidth: 1,
        borderColor: Colors.dark.tintAudiobook,
        borderRadius: 10,
        backgroundColor: Colors.dark.background
    },
    epikuretAudioTitle: {
        color: Colors.dark.tintAudiobook
    },
    epikuretAudioSubtitle: {
        color: color(Colors.dark.tintAudiobook).lighten(0.15).hex()
    },
    title: {
        color: Colors.dark.tint
    },
    date: {
        color: Colors.dark.text
    },
    dialog1: {
        color: Colors.dark.text
    },
    dialog2: {
        color: Colors.dark.text
    },
    thought: {
        color: Colors.dark.text
    },
    html: {
        color: Colors.dark.text
    },
    buttonGroupButton: {
        backgroundColor: 'transparent'
    },
    buttonGroupSelectedButton: {
        backgroundColor: Colors.dark.buttonGroupSelection
    },
    buttonGroupSelectedText: {
        color: 'black'
    },
    buttonGroupText: {
        color: Colors.dark.buttonGroupSelection
    },
    buttonGroupBorder: {
        color: Colors.dark.buttonGroupSeparator
    },
    searchBar: {}
};
