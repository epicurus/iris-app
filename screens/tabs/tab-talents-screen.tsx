import * as React from 'react';
import { Image, StyleSheet } from 'react-native';

import { Text, View } from '../../components/Themed';
import testImage from '../../assets/images/image.jpg';

export default function TabTextsScreen(): JSX.Element {
    return (
        <View style={styles.container}>
            <Text style={{ color: '#888', fontSize: 30 }}>I try something...</Text>
            <Image source={testImage} style={{ width: 305, height: 159 }} />
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#000',
        alignItems: 'center',
        justifyContent: 'center'
    }
});
