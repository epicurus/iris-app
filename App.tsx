import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { Text } from 'react-native-elements';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { AppearanceProvider } from 'react-native-appearance';
import { ThemeProvider } from 'react-native-elements';
import useCachedResources from './hooks/useCachedResources';
import useColorScheme from './hooks/useColorScheme';
import Navigation from './navigation';
import * as Localization from 'expo-localization';
import i18n from 'i18n-js';

i18n.locale = Localization.locale;
i18n.fallbacks = true;

console.info('LOCALE: ', i18n.locale);

export default function App(): JSX.Element {
    const isLoadingComplete = useCachedResources();
    const colorScheme = useColorScheme();

    if (!isLoadingComplete) {
        return <Text>Loading ...</Text>;
    } else {
        return (
            <SafeAreaProvider>
                <AppearanceProvider>
                    <ThemeProvider theme={theme}>
                        <Navigation colorScheme={colorScheme} />
                        <StatusBar />
                    </ThemeProvider>
                </AppearanceProvider>
            </SafeAreaProvider>
        );
    }
}

const theme = {
    colors: {
        primary: 'darkblue',
        secondary: 'black',
        grey: 'grey',
        success: 'green',
        error: 'red',
        warning: 'orange'
    }
};
