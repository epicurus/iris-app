import epikuretImage0 from '../images/2bbfe9ebef6074125834cb0e4a91aff3b700d94d.jpg';
import epikuretImage1 from '../images/5bb62d6c8fb559471ad6b7f778a3711cf1d517bf.jpg';
import epikuretImage2 from '../images/aadbcafba46e94a4ab184c142644a0540a09acba.jpg';
import epikuretImage3 from '../images/30b27002ea9821fe4a902fa8afbc9084583aacd3.jpg';
import epikuretImage4 from '../images/a36376dd700419546de7e37c49338a15d334b736.jpg';
export default [
  {
    "epikuretId": "02-Reading-the-athmosphere-in-zoom",
    "title": "sv-SE",
    "type": "epicurus",
    "audio": "https://storage.googleapis.com/ewer/_home_michael_Der-Garten-des-Epikur_storage_Garden-Of-Epicurus_2021-02-23_02-Reading-the-athmosphere-in-zoom_sv-SE.mp3",
    "promotion": "home",
    "subtitle": "Epikurus trädgård, 23. feb. 2021",
    "dialog": [
      "Inledningen sa du att det då Då tittar i atmosfären och läser där hur hur fungerar det här över sin.",
      "Iris: Precis som om vi är en meter ifrån varandra eller om någon mer i Australien när jag är i Sverige atmosfären har det här och nu närvarande hela tiden Det är Det mörka i universum(???) Där finns inget avstånd och det finns inte heller tid och rum utan det är bara här och nu var närvarande information från mänsklighetens barndom och till mänsklighetens undergång finns i atmosfären att det kanske finns ifrån andra också men det är inte mitt språk så det kan jag inte men men det är det en vetskap Det är inte så att jag ser att man ser inom citationstecken bara en vetskap som jag kallar för att se för att jag ska på något sätt blir begripligt och inte någon slags mystiskt magiskt Bokus"
    ]
  },
  {
    "epikuretId": "12-Energy to work",
    "title": "### TITLE ###",
    "type": "epicurus",
    "audio": "https://storage.googleapis.com/ewer/_home_michael_Der-Garten-des-Epikur_storage_Garden-Of-Epicurus_2021-02-23_12-Energy to work_sv-SE.mp3",
    "promotion": "home",
    "subtitle": "Epikurus trädgård, 23. feb. 2021",
    "dialog": [
      "Jag har jag har kommit i kontakt med fenomenet att människor berättar att att förr kunde de arbetar hela dagen Och att jag undrar om det kanske har att göra med den tredje IV kommunikations dimensionen att det här inte riktigt passar ihop längre eller vad vad vad kan det hänga ihop med",
      "Iris: Det hänger ihop med våran föreställning i väldigt väldigt snäll och en annan lösning När du säger att när du pratar om energi så pratar de som om det fanns en viss mängd energi att den ska fördelas och det är det sättet majoritetsmissförstånd men det är också en anpassning och i det ögonblicket det blir rädd för att inne (???)inte ska räcka så blir kroppen och det har du inte tillgång på din energi när du har tillit till att du har tusen gånger mer energi än vad du kommer energin i rörlighet hela tiden Och då känner du att du har kraft så fort de tror att det bara finns en viss mängd och blir rädd för att väcka oss lokalen då kan du inte känna till honom att det är skälet till att människor blir rädda och förlamad och inte har tillgång på sin Kraft och dessvärre så blir den mindre och mindre då Alltså när när när vi inte litar på att jag har tusen gånger mer kraft än vad vi behöver Utan tror att kraften kan ta slut så kommer vi att du kommer rädslan tidigare och tidigare vilket gör att vi inte har tillgång på våran överskottsenergi så det är om vi går till sist Om vi gör allt som är nödvändig utan att vara bekväma utan att vi verkligen obekväma saker hela tiden Då får vi Då får vi tillgång på all den Kraft och energi därför att då blir kroppen nöjd ses och då har vi har mycket kraft men det är ungefär detsamma som att om man fryser om man håller sig stilla och klipper in för att man tror att om jag bara håller med stilla och har någonting på mig så blir jag varm intressant då blir man vara kallare och kallare om man istället då sätter igång att springa fram och tillbaka eller spela och sjunga och röra sig dansa allt möjligt Och sen Kryper in i det där varma då stannar kvar för då håller kroppen värme som gör att om man har gått bra hölje som är håller man värmen till exempel över hela natten (???)har lärt sig att energi skapas på det sättet då Kommer man att upptäcka Vilken enorm överskottsenergi man har även när man har arbetat hela 20 timmar eller så där som jag gör nu ska vi inte prata om det fungerar betesmark om man men ändå för mycket på den sidan istället för bensin vi har Våra fel och brister balans är det som gäller."
    ]
  },
  {
    "epikuretId": "01-indifference",
    "title": "### TITEL ###",
    "type": "epicurus",
    "audio": "https://storage.googleapis.com/ewer/_home_michael_Der-Garten-des-Epikur_storage_Garden-Of-Epicurus_2021-03-23_01-indifference_sv-SE.mp3",
    "promotion": "home",
    "subtitle": "Epikurus trädgård, 23. mars 2021",
    "dialog": [
      "Jag jag sysselsätter mig sedan en längre tid tillbaka med benet likgiltighet att människor är likgiltiga inte berörs av ett fenomen bordsvävstol Kom hit så förbannat med den 24 från det har vi läst din vit My wife is men jag alltså (???)likgiltighet kärlek tillståndet det som är det naturligt för oss att människor",
      "Iris: Alltså Kärleken är ett tillstånd som finns i hela världen och den finns i hela tiden Det är ingen känsla utan blir tillstånd men det går väldigt lätt att dra till känsla så fort man låter ett sinne roa sig på något sätt kärlek till bandet kärlek när man tar upp det till känsla (???)så blir det så blir man upp en och sårbara det blir massor med olika känslor inuti en själv Och då är det så att om det om om den på ett eller annat sätt kapsejsar så går den över till hat och det är den Det är motsatsen på samma pinne så att det är på samma balans pinne grund både kärlek och hat och alla de känslor som finns inom de spektrumet när man drar upp kärlek till känsla de hem dem i har livskraft det är lyftkraften så att det är som som alstrar det och andra sidan (???)på andra sidan det andra tillståndet det är likgiltighet (???) det är när de av ett eller annat skäl står i vägen saker och ting för att släppa in tillståndet kärlek och leva i allt det som som tillståndet kärlek anfaller oss med när vi lämnat själv då hamnar vi i likgiltighet"
    ]
  },
  {
    "epikuretId": "04-advertizment-and-tiredness",
    "title": "TITLE!",
    "type": "epicurus",
    "audio": "https://storage.googleapis.com/ewer/_home_michael_Der-Garten-des-Epikur_storage_Garden-Of-Epicurus_2021-03-23_04-advertizment-and-tiredness_sv-SE.mp3",
    "promotion": "home",
    "subtitle": "Epikurus trädgård, 23. mars 2021",
    "dialog": [
      "Maria blir väldigt trött Hon märker att han blir trött när hon går omkring i staden och ser reklam och och andra saker och och hon känner av det här att det förväntas att utöva påtryckningar på henne att hon ska inreda sitt hem på något annat sätt ska se ut på annat sätt Om du skulle kunna säga någonting om om Varför blir hon tröttnar Hon ser många saker",
      "Iris: det som du reagerar på eller det du blir trött av är att all reklam som finns runt omkring dig allting som är produkt inrikta och som förstärks med ljus med färger med med ljud rörelser och så vidare det tröttar våra sinnen så vi kan inte låta bli att de att de gjort så för att vi inte ska kunna låta bli att pengar att det är så lustigt gjort så att det är meningen att de inte ska gå in på Ditt medvetna Persson på Ditt medvetna från så är det meningen att att du egentligen bara ska bli accepterad och avvisa tycka illa om eller tycker (???)spelar ingen roll Det är bara att du reagerar därför att då skapas en en etsning skulle man säga för mannen Vad ska jag säga det blir som en tatuering inuti så du kommer in i en affär och ska handla Och så ser du ett sånt märke som du har blivit tatuerad med så ska du dras till det för att köpa just det märket att det är Vad ska jag säga (???)det blir en viss mån anpassade till det och Allan passning På ett eller annat sätt blir den tröttheten likgiltigheten sorg"
    ]
  },
  {
    "epikuretId": "14-World-situation-changed-since-covid",
    "title": "sv-SE",
    "type": "epicurus",
    "audio": "https://storage.googleapis.com/ewer/_home_michael_Der-Garten-des-Epikur_storage_Garden-Of-Epicurus_2021-03-23_14-World-situation-changed-since-covid_sv-SE.mp3",
    "promotion": "home",
    "subtitle": "Epikurus trädgård, 23. mars 2021",
    "dialog": [
      "(???)det handlar om alltså frågan med med situationen som vi lever i alltså världs situationen om hur Vad tänker du är bästa sättet att förhålla sig till allt som pågår för att jag och sen krona först kom att jag att jag börjar läsa på mer och mer och upptäcker att Ja men det är mycket som inte riktigt stämmer överens med verkligheten så vitt jag kan se hur kan man Hur kan man förhålla sig till allt som händer i lördags var jag där det står på för första gången på en demonstration i Stockholm För jag gick dit för demokratins skull för att jag känner att demokratin ganska lättvindigt i hotad till och med i Sverige (???)som du är liksom filmat av Nalle porter och sen var liksom inom några timmar var 14 500 ilskna kommentarer för att jag hotar men osv Det blir liksom många frågor",
      "Iris: Det var bara det inte bara men jag lämnade där och sen tar du på tyska alltså jag har precis idag nästan redigerat färdigt efter som heter s och där får du ett utförligt svar på den fråga att samtala om s ätliga heter den men jag ska svara dig ändå lite grann på det för att Med den oerhört intressant fråga det som vi kallar för verkligheten det här (???)om den materiella verkligheten alltså allting som går att objektifiera och göra en sak och i det så har vi skapat ett nollsummespel vi har skapat en en ekonomisering som som är noll som är spel till skillnad mot de primära (???)så är det här sekundärt elementärt och valbart det styrbart utav människor i och med att det är ett spel som finns det spelregler att det finns vinst och förlust och det är att mina betyder att någon förlorar så att säga att jag med det Det är den skevheten som du sa det är den Det är det som stör dig och det är också Det som du får en känsla av att det hotar demokratin och de gör det så det är alldeles sant bygger på ett omänskligt tänkande och det är de som skorrar i dig Tror jag för att kunna förstå det så behöver man veta utgångspunkten att det är vårat medvetande i grunden är en skyddszon försvarsmekanism för att överleva och vi har fått den där för att vi vi har egentligen ingen förutsättning för att överleva i Den vilda naturen som enskilda människor vi har för Tobias innan så vi ser inte till jättemycket hör inte tillräckligt mycket känner inte dofter Och så står det inte på saker i naturen hjälplösa väldigt lång tid innan innan vi blir kapabla till någonting alls egentligen men vi är kapabla så klarar vi oss i alla fall inte i Den vilda naturen för att vi springer för sakta vi slåss för dåligt vi (???)Vi har ingen chans emot vilda rovdjur utan de vi blir ett lätt byte (???) och därför har vi fått ett medvetande och ett språk organ och att vi behöver vara i en flock att kunna förmedlas med varandra Vad är det då som utmärker det mänskliga och det är en sak och det är att man när man är i en grupp för att kunna skydda sig kunna överleva i Den vilda naturen så så ser man till att ingen förlorar det är det som utmärker ursprungsbefolkningar överallt där att man inom gruppen ta hand om varandra på ett sådant sätt så att ingen förlorar det som skapar mer hållbart (???) ett hållbart en hållbar att leva i tillsammans som människa när man går tittar på hur hur hur vårt samhälle hur vi har när våra liv inte hotat när vi inte längre lever i Den vilda naturen utan att vi lyckas organisera bort hur vi då använder vårat medvetande intellektuellt istället och tror att vi har makten över liv då kan styra på på ett sätt som inte fungerar och det skrämmer våra kroppar och det är det du känner av tror (???) och ett ekonomi ser att system är ett omänskligt system att det blir en kan man säga en tjänst som en ett känslomässigt spel det bästa känslomässiga spelet de mest existentiell Avesta känslomässiga spelet som pågår i våran värld det är krig och det går ut på att skrämma en människa till att bli fullständigt omänskligt utan att den personen vet att den är rädd Det är det som är radikalism någonstans så tror jag att att det här att du känner av dig att du känner dig hotad av att det är det skorrar någonstans inuti dig även om du kanske det men du får fundera på det i alla fall kan titta på de",
      "menar att jag på sätt och vis blir radikaliserad kanske",
      "Iris: inte att du blir högkvalificerad utan snarare att att de hotas av den radikaliseringen som sker i samhället det jag menar dessvärre Har du också rätt i att vi alla när vi blir tillräckligt utredda så blir radikaliserade och så att det går inte helt säger som lever i helvete"
    ]
  },
  {
    "epikuretId": "epicurus-introduction-2021",
    "title": "Epikuros Trädgård - Virtuellt",
    "type": "thought",
    "origin": "ksk",
    "year": "2021",
    "promotion": "none",
    "subtitle": "Kommunikation som konst, 2021",
    "thought": [
      "Epikuros var en filosof som levde några hundra år före vår tideräkning i Grekland. Han tillhörde överklassen men han hade en helt annat inställning till livet än den som rådde bland andra filosofer. Han samlade folk som var intresserade i sin stora park och satt ner med dem och samtalade om livet och livets mening i att leva samt att hitta balans inuti sig själv för att leva ett nöjsamt liv och det kallande han att ’må bra’. En nidbild i efterhand som han blev beskylld för var Hedonismen, en ohejdad njutningslystnad som var något helt annorlunda än Epikuros balanstänkande och den handlade om att optimera och mäta berusningseffekten i kickarna man kunde få känslomässigt, helst erotiskt eller genom exalterat våld. Den kom till för att krossa epikuréernas pacifistiska hållning och balans i samtal och ömsesidighet och den fridsamma tillfredsställelsen i att må bra i detta, en form av hälsosamt enkelt leverne. Själv hade Epikuros svår reumatism så varje morgon kom det studenter, bar ut honom och lade honom i solen och när han var varm lades han på en sträckbänk och drogs ut och då myntades ett bevingat ord, ”till och med på sträckbänken kan man uppleva ett uns av lycka” Det berodde på att när han drogs ut fanns det kuggar och varje gång de släppte taget så släppte sträcket lite och det var ett ögonblick av smärtfrihet och det var då han kände ett ögonblick av lycka. När jag läste filosofi fann jag honom och han stod för allt det jag själv hade kommit fram till så jag är hans ’vapendragare’ jag går i hans fotspår och tar hand om de som kommer i min väg genom att följa hans tradition och upprätta hans trädgård, just nu internationellt på ’nätet’. Så… om du kommer till min epikuros trädgård så kan du komma med vilka frågor som du är upptagen med inuti, sådant som du inte tror att det går att tala om eller få något nytt inspel på. . . privar, personligt, filosofiskt mm, det som du själv inte har något svar på men gärna vill ha spegling på. Jag tar alla frågor seriöst och ibland säger jag självklart att jag inte vet, men ofta kommer det ut information i atmosfären som jag kan fånga och förmedla. Det är det som är att filosofera och det är så intressant och så välkommen. Du kan också komma bara för att lyssna och kanske komma på något att fråga om, eller inte, sidoeffekten av ditt lyssnande finns i den atmosfär som bildas och berikar på samma sätt som frågor, så. . . kom gärna bara för att du är nyfiken. Ibland blir det också lite primärarbete för att spegla Epikuros sätt att fungera med aktning och respekt, att vara i tillit i sig själv, gemensamhet med andra och i inre trygghet. Denne filosof tog mycket betalt av de som var besuttna och han lät kvinnor och slavar vara med gratis och stundom få ett mål mat och en klädeståga på köpet. Denna tradition upprätthåller jag gärna genom samordnande av andra, just nu Michael och Benedicte i Tyskland och samordnare Filip Wall i Sverigt."
    ]
  },
  {
    "epikuretId": "thoughts-primary-reference",
    "title": "Primärer Bezugspunkt",
    "type": "thought",
    "origin": "ksk",
    "year": "2013",
    "promotion": "none",
    "subtitle": "Kommunikation som konst, 2013",
    "thought": [
      "REFERENSPUNKT, några exempel att öva sig själv på.",
      "När du är dig själv som dig själv och står framför spegeln och din bild i spegeln är din ’publik’, som du talar till och du tittar, och känner efter, hur du reagerar på det du själv säger till dig.",
      "• Livet är livets mening och jag lever och är här och nu framför denna spegel och jag känner mig lite .... och då ser jag ut så här.",
      "• Livets värde finns i att jag finns och jag finns... Jag kan vila i det, i mig själv och känna mig ok.. med det... Går inte att lägga till något eller dra ifrån något, det är....",
      "• Gemenskap är jag i... med mig själv om jag inte är rädd... och inte värderar mig själv... varken uppåt eller neråt, bara är...",
      "• Trygghet är jag i, grundtillstånd, när jag vet att mitt liv inte är hotat... och när jag inte tror att jag måste få något utifrån... för att vara i den. Jag är vuxen, över 20 år, och då är jag färdigväxt... och kan koppla inuti till att jag är kapabel att klara mig själv... oberoende av att någon utifrån räddar mig... är här och nu, ohotad....",
      "• Släpp tanken på att göra rätt... eller vara duktig... och vänd på det. Det är rätt och du är duktig när du gör ingenting... och när du gör det vardagligt nödvändiga utan att värdera det som tungt eller tråkigt... utan att värdera... Om du då upptäcker vad som är dåligt eller som du tycker är fel, så sätt fokus på det... och sedan, se vad du kan lära. Lär, lär, lär och försonas med att så är det i verkligheten... Låt din nöjdhet komma inifrån när du lever detta.",
      "• Nästa steg är att rätta till felen om det finns något att rätta till. Vissa rättar till sig själva bara genom medvetenheten och att man inte upprepar... andra går inte att rätta till och istället får man nöja sig med att försonas... med att det blev som det blev... En del kan man ändra i verkligheten... åsikter... rikningar... och ha förståelse, samt konkret rätta till...",
      "• Ständigt veta att fel blir det... eftersom livet kräver ofullständighet för att kunna finnas... ’Jag’ är inte roten till det, bara ofullständig och det gäller också för ’andra’, de är inte roten till det onda, bara ofullständiga åstadkommer det.",
      "• Kroppen producerar känslor för att vi ska kunna förändra våra föreställningar... de som inte längre stämmer i verkligheten. När vi mister någon så blir vi lessna och gråter och sörjer tills vi laddat ur det kroppen producerat och sedan blir vi lite vemodiga... Då har föreställningen som vi hadde, att det fanns som inte längre finns... blir möjligt att leva förutan... att det fattas oss och så småningom faller i glömska. Det gäller rädsla, ilska och glädje också.",
      "• Intimitet är.... det råder när vi bara är i oss själva, i varandet, här och nu.... i oss själva... i mötet med andra... i realtion till allt vi har omkring oss.... i sammnahang vi ingår i... osv. Då blir kroppen glad och vi drar upp kärlek till känsla... vi känner livet levande...",
      "• Smärta uppstår i alla känslor... smärta är kroppens naturliga sätt att kommunisera. Den höjer medvetenheten om livets ofullständighet och ger oss utrymme att förstå livets villkor....",
      "• Smärta är vår bundsförvant och vi dör inte av den... den står i livets tjänst...",
      "• Autopilot, det skydds- flykt- och försvarssystem som klickar in när kroppen tror att den är i livsfara... gör ingenting... stanna upp... se dig om här och nu... konstatera om det är livsfarligt... om inte, ta tid på dig och bli medveten om att du är rädd.",
      "• När det är fara så kommer du att automatiskt följa de impulser som kommer från autopiloten och de kommer när de inte är relevanta också och då behöver man stanna upp... bli medveten om här och nu situationen, registrera orfarligheten, och... tänka om...",
      "• FÖRÄLDRING",
      "ATT VARA ETT MED OSS SJÄLVA, VAD DET BETYDER I LIVET.",
      "VÄL BEKOMMET"
    ]
  }
]