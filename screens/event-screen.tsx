import React from 'react';
import { ScrollView, FlexStyle, Pressable } from 'react-native';
import { StackScreenProps } from '@react-navigation/stack';
import _ from 'lodash';

import { noEvent } from '../hooks/useGoogleCalendar';
import Event from '../components/Event';
import useStyles from '../hooks/useStyles';
import { RootStackParamList, TopTabParamList } from '../types';

type Props = StackScreenProps<RootStackParamList & TopTabParamList, 'Event'>;

export default function EventScreen({ navigation, route }: Props) {
    const { event } = route.params;
    const styles = useStyles(commonStyles);

    return (
        <ScrollView style={styles.container}>
            <Pressable onPress={() => navigation.goBack()}>
                <Event event={event || noEvent} details />
            </Pressable>
        </ScrollView>
    );
}

const commonStyles: { [key: string]: FlexStyle } = {
    container: {
        width: '100%'
    }
};
