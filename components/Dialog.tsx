import React from 'react';
import useStyles from '../hooks/useStyles';
import { Text, ViewStyle, TextStyle } from 'react-native';

const isSpeaker = (line: string) => {
    return line.startsWith('Iris:');
};

const withoutSpeaker = (line: string) => {
    return line.substr(6);
};

type Props = {
    dialog: string[];
    details: boolean;
};

const Dialog = ({ dialog }: Props): JSX.Element => {
    const styles = useStyles(commonStyles);

    return (
        <>
            {dialog.map((line, i) => {
                const style = isSpeaker(line) ? styles.dialog1 : styles.dialog2;

                return (
                    <Text key={i} style={style}>
                        {isSpeaker(line) ? withoutSpeaker(line) : line}
                    </Text>
                );
            })}
        </>
    );
};

const commonStyles: { [key: string]: ViewStyle & TextStyle } = {
    dialog1: {
        fontFamily: 'PTSans_Regular',
        fontSize: 20,
        lineHeight: 26,
        textAlign: 'justify',
        paddingTop: 10,
        paddingBottom: 10
    },
    dialog2: {
        fontFamily: 'TheGirlNextDoor_Regular',
        fontSize: 20,
        lineHeight: 26,
        textAlign: 'justify',
        paddingTop: 10,
        paddingBottom: 10
    }
};

export default Dialog;
