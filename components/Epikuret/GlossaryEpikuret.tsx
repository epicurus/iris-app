import React, { useCallback, useState, useMemo } from 'react';
import { Text, View, useWindowDimensions, ViewStyle, TextStyle } from 'react-native';
import { Card, ButtonGroup } from 'react-native-elements';
import { Ionicons } from '@expo/vector-icons';
import AppLoading from 'expo-app-loading';
import { useFonts } from 'expo-font';
import _ from 'lodash';
import { Video, AVPlaybackStatus } from 'expo-av';

import useStyles from '../../hooks/useStyles';
import { EpikuretDate } from '../../types';
import buttonGroupProps from '../../constants/ButtonGroupProps';
type Props = {
    epikurets?: EpikuretDate[];
    details?: boolean;
    promotion?: 'home';
};

export default function Epikuret({ epikurets, details = false }: Props): JSX.Element {
    const video = React.useRef<Video>(null);
    const [status, setStatus] = React.useState<AVPlaybackStatus | null>(null);
    const [authorIndex, setAuthorIndex] = useState<number>(0);
    const epikuret = epikurets && epikurets[authorIndex];
    const authorButtons = useMemo(
        () =>
            epikurets?.map((epikuret, index) => {
                return {
                    element: () => (
                        <View
                            style={{
                                flexDirection: 'row',
                                justifyContent: 'space-between',
                                alignItems: 'center'
                            }}
                        >
                            <Text style={{ paddingRight: 7 }}>{epikuret.author}</Text>
                            {index === authorIndex ? (
                                <Ionicons
                                    name={
                                        status && status.isLoaded && status.isPlaying
                                            ? 'md-pause-circle-sharp'
                                            : 'md-play-circle-sharp'
                                    }
                                    size={buttonGroupProps.iconSize}
                                />
                            ) : null}
                        </View>
                    )
                };
            }),
        [epikurets, status, authorIndex]
    );

    const {
        title,
        subtitle,
        video: uri = ''
    } = epikuret || {
        title: 'Epikuret not found ...'
    };

    const styles = useStyles(commonStyles);
    const width = useWindowDimensions().width - 10;

    const [fontsLoaded] = useFonts({
        TheGirlNextDoor_Regular: require('../../assets/fonts/TheGirlNextDoor-Regular.ttf'),
        PTSans_Regular: require('../../assets/fonts/PTSans-Regular.ttf')
    });

    const onPressAuthor = useCallback(
        (index: number) => {
            if (status && !status.isLoaded) {
                return;
            }
            if (index === authorIndex) {
                if (status && status.isPlaying) {
                    video?.current?.pauseAsync();
                } else {
                    video?.current?.playAsync();
                }
            } else {
                video?.current?.pauseAsync();
                setStatus(() => null);
                setAuthorIndex(index);
            }
        },
        [setAuthorIndex, authorIndex, status]
    );

    if (!epikuret) return <Text>Epikuret not found.</Text>;

    if (!fontsLoaded) {
        return <AppLoading />;
    }

    if (details) {
        commonStyles.epikuretGlossaryTitle.fontSize = 28;
        commonStyles.epikuretGlossaryTitle.lineHeight = 36;

        return (
            <Card containerStyle={{ width, ...styles.epikuretGlossaryCard }}>
                <Card.Title style={styles.epikuretGlossaryTitle}>{title}</Card.Title>
                <Video
                    style={{ height: (width / 16) * 9 }}
                    ref={video}
                    source={{
                        uri
                    }}
                    resizeMode="contain"
                    isLooping
                    onPlaybackStatusUpdate={(status: AVPlaybackStatus) => {
                        setStatus(() => status);
                    }}
                />
                <ButtonGroup
                    onPress={(index: number) => {
                        onPressAuthor(index);
                    }}
                    disabled={!status || !status.isLoaded}
                    buttons={authorButtons}
                    selectedIndex={authorIndex}
                    containerStyle={{
                        height: buttonGroupProps.height,
                        borderWidth: 0
                    }}
                    buttonContainerStyle={{
                        height: buttonGroupProps.height,
                        minWidth: 75,
                        backgroundColor: 'transparent',
                        borderColor: styles.epikuretGlossaryCard.backgroundColor
                    }}
                    buttonStyle={{
                        backgroundColor: styles.epikuretGlossarySubtitle.color
                    }}
                    selectedButtonStyle={{
                        backgroundColor: styles.epikuretGlossarySubtitle.color
                    }}
                    innerBorderStyle={{
                        color: 'yellow'
                    }}
                    textStyle={{
                        color: 'yellow'
                    }}
                    selectedTextStyle={{
                        color: 'black',
                        fontSize: 40
                    }}
                />
            </Card>
        );
    } else {
        commonStyles.epikuretGlossaryTitle.fontSize = 22;
        commonStyles.epikuretGlossaryTitle.lineHeight = 32;
        return (
            <Card containerStyle={{ width, ...styles.epikuretGlossaryCard }}>
                <Card.Title style={styles.epikuretGlossaryTitle}>{title}</Card.Title>
                <Card.FeaturedSubtitle style={styles.epikuretGlossarySubtitle}>
                    {subtitle}
                </Card.FeaturedSubtitle>
            </Card>
        );
    }
}

const commonStyles: { [key: string]: ViewStyle & TextStyle } = {
    epikuretGlossaryCard: {
        marginTop: 15,
        width: '95%'
    },
    epikuretGlossaryTitle: {
        fontWeight: 'normal',
        fontFamily: 'TheGirlNextDoor_Regular',
        textAlign: 'center',
        paddingTop: 20
    },
    epikuretGlossarySubtitle: {
        fontWeight: 'normal',
        fontFamily: 'TheGirlNextDoor_Regular',
        textAlign: 'center',
        fontSize: 16
    },
    epikuretGlossaryVideo: {},
    epikuretGlossaryVideoButton: {}
};
