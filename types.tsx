import { EventDate } from './hooks/useGoogleCalendar';
import { AVPlaybackSource } from 'expo-av/build/AV';

export type RootStackParamList = {
    Root: undefined;
    NotFound: undefined;
    Event: { event: EventDate | null };
    Epikuret: { epikurets: EpikuretDate[]; details: boolean };
};

export type TopTabParamList = {
    Home: undefined;
    Iris: undefined;
    Writings: undefined;
    Events: undefined;
};

export type TabHomeParamList = {
    TabHomeScreen: undefined;
};

export type TabIrisParamList = {
    TabIrisScreen: undefined;
};

export type TabTextsParamList = {
    TabTextsScreen: undefined;
};

export type TabTalentsParamList = {
    TabTalentsScreen: undefined;
};

export type TabEventsParamList = {
    TabEventsScreen: undefined;
};

export type BuyProp = {
    [key: string]: { provider: string, url: string }[] };
};

export type EpikuretDate = {
    epikuretId: string;
    type: 'thought' | 'epicurus' | 'book' | 'ebook' | 'audiobook' | 'glossary' | 'video';
    title: string;
    year: string;
    author: string;
    subtitle: string;
    text?: string;
    dialog?: string[];
    thought?: string[];
    audio?: string;
    video?: string;
    promotion?: string;
    book?: string;
    ebook?: string;
    audiobook?: string;
    cover?: typeof Image;
    buy?: BuyProp;
};

export type Epikurets = EpikuretDate[];

type UI = {
    navigation: unknown;
    screens: {
        [key: string]: { [key: string]: string };
    };
};

export type LanguageContent = { epikurets: Epikurets; ui: UI };

export type Content = {
    [key: string]: LanguageContent;
};
