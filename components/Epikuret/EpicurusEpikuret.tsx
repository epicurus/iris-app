import React from 'react';
import { Text, useWindowDimensions, ViewStyle, TextStyle } from 'react-native';
import { Card } from 'react-native-elements';
import AppLoading from 'expo-app-loading';
import { useFonts } from 'expo-font';
import _ from 'lodash';

import useStyles from '../../hooks/useStyles';
import { EpikuretDate } from '../../types';
import AudioPanel from '../AudioPanel';
import Dialog from '../Dialog';

type Props = {
    epikurets?: EpikuretDate[];
    details?: boolean;
    promotion?: 'home';
};

export default function Epikuret({ epikurets, details = false }: Props): JSX.Element {
    const epikuret = epikurets && epikurets[0];
    if (!epikuret) return <Text>Epikuret not found.</Text>;

    const { title, subtitle, audio, dialog } = epikuret || {
        title: 'Epikuret not found ...'
    };

    const styles = useStyles(commonStyles);
    const width = useWindowDimensions().width - 10;

    const [fontsLoaded] = useFonts({
        TheGirlNextDoor_Regular: require('../../assets/fonts/TheGirlNextDoor-Regular.ttf'),
        PTSans_Regular: require('../../assets/fonts/PTSans-Regular.ttf')
    });

    if (!fontsLoaded) {
        return <AppLoading />;
    }

    const audioPanel = details ? <AudioPanel audio={audio || ''} /> : null;

    if (details) {
        commonStyles.epikuretEpicurusTitle.fontSize = 28;
        commonStyles.epikuretEpicurusTitle.lineHeight = 36;

        return (
            <Card containerStyle={{ width, ...styles.epikuretEpicurusCard }}>
                <Card.Title style={styles.epikuretEpicurusTitle}>{title}</Card.Title>
                {audioPanel}
                <Dialog dialog={dialog || []} details={details} />
            </Card>
        );
    } else {
        commonStyles.epikuretEpicurusTitle.fontSize = 22;
        commonStyles.epikuretEpicurusTitle.lineHeight = 32;
        return (
            <Card containerStyle={{ width, ...styles.epikuretEpicurusCard }}>
                <Card.Title style={styles.epikuretEpicurusTitle}>{title}</Card.Title>
                <Card.FeaturedSubtitle style={styles.epikuretEpicurusSubtitle}>
                    {subtitle}
                </Card.FeaturedSubtitle>
            </Card>
        );
    }
}

const commonStyles: { [key: string]: ViewStyle & TextStyle } = {
    epikuretEpicurusCard: {
        marginTop: 15,
        width: '95%'
    },
    epikuretEpicurusTitle: {
        fontWeight: 'normal',
        fontFamily: 'TheGirlNextDoor_Regular',
        textAlign: 'center',
        paddingTop: 20
    },
    epikuretEpicurusSubtitle: {
        fontWeight: 'normal',
        fontFamily: 'TheGirlNextDoor_Regular',
        textAlign: 'center',
        fontSize: 16
    }
};
