import { Epikurets, Content } from '../types';

import deEpikurets from './de-DE/epikurets';
import deUi from './de-DE/ui.json';
import enEpikurets from './en-GB/epikurets';
import enUi from './en-GB/ui.json';
import svEpikurets from './sv-SE/epikurets';
import svUi from './sv-SE/ui.json';

const content: Content = {
    default: {
        ui: enUi,
        epikurets: (enEpikurets as unknown) as Epikurets
    },
    'de-DE': {
        ui: deUi,
        epikurets: (deEpikurets as unknown) as Epikurets
    },
    'en-GB': {
        ui: enUi,
        epikurets: (enEpikurets as unknown) as Epikurets
    },
    'en-US': {
        ui: enUi,
        epikurets: (enEpikurets as unknown) as Epikurets
    },
    'sv-SE': {
        ui: svUi,
        epikurets: (svEpikurets as unknown) as Epikurets
    }
};

export const Locales: string[] = ['de-DE', 'en-GB', 'sv-SE' /*, 'lt-LT', 'cs-CZ'*/];
export const LocalesAliases: { [key: string]: string } = {
    default: 'en-GB',
    'de-DE': 'de-DE',
    'en-GB': 'en-GB',
    'sv-SE': 'sv-SE',
    'en-US': 'en-GB',
    'no-NO': 'sv-SE',
    'da-DK': 'sv-SE',
    de: 'de-DE',
    en: 'en-GB',
    sv: 'sv-SE'
};
export const LocalesLabels: { [key: string]: string } = {
    'de-DE': 'deutsch',
    'en-GB': 'english',
    'sv-SE': 'svenska'
};

export const LocalesDefault = 'en-GB';

export default content;
