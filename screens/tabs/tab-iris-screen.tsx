import * as React from 'react';
import { StyleSheet } from 'react-native';
import { FlatGrid } from 'react-native-super-grid';
import { Text, View } from '../../components/Themed';

export default function TabIrisScreen(): JSX.Element {
    return (
        <View style={styles.container}>
            <FlatGrid
                itemDimension={130}
                data={[1, 2, 3, 4]}
                renderItem={({ item }) => <Text>{item}</Text>}
            />
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: 20,
        backgroundColor: '#000',
        alignItems: 'center',
        justifyContent: 'center'
    }
});
