import React, { useCallback, useState, useEffect, useMemo } from 'react';
import { ScrollView, FlexStyle, ViewStyle, Pressable } from 'react-native';
import { SearchBar, ButtonGroup } from 'react-native-elements';
import { StackScreenProps } from '@react-navigation/stack';
import { TopTabParamList, RootStackParamList, EpikuretDate } from '../../types';

import Epikuret, { epikuretTypes } from '../../components/Epikuret';
import useStyles from '../../hooks/useStyles';
import useContent from '../../hooks/useContent';

const KEY_TIMER_MS = 300;
const MAX_EPIKURETS_ON_PAGE = 100;

type Props = StackScreenProps<TopTabParamList & RootStackParamList, 'Home'>;

export default function TabTextsScreen({ navigation }: Props): JSX.Element {
    const { ui, epikurets } = useContent();
    const [searchTerm, setSearchTerm] = useState<string>('');
    const [keyTimer, setKeyTimer] = useState<NodeJS.Timeout | null>(null);
    const [selectedType, setSelectedType] = useState<number>(0);
    const [epikuretsOfType, setEpikuretsOfType] = useState<EpikuretDate[]>([]);
    const [matchingEpikurets, setMatchingEpikurets] = useState<EpikuretDate[]>(
        epikurets.slice(0, MAX_EPIKURETS_ON_PAGE)
    );
    const styles = useStyles(commonStyles);

    const filterEpikurets = (): EpikuretDate[] => {
        const filteredEpikurets = epikurets.filter(
            (epikuret) => epikuret.type === epikuretTypes[selectedType]
        );
        setEpikuretsOfType(filteredEpikurets);

        return filteredEpikurets;
    };

    const updateSearch = useCallback(
        (term: string) => {
            setSearchTerm(term);

            if (keyTimer) {
                clearTimeout(keyTimer);
            }
            setKeyTimer(
                setTimeout(() => {
                    setKeyTimer(null);
                }, KEY_TIMER_MS)
            );
        },
        [setSearchTerm]
    );

    useEffect(() => {
        setMatchingEpikurets(
            epikuretsOfType
                .filter((epikuret) => {
                    return (
                        !searchTerm ||
                        JSON.stringify(epikuret)
                            .toLowerCase()
                            .includes(searchTerm.toLocaleLowerCase())
                    );
                })
                .slice(0, MAX_EPIKURETS_ON_PAGE)
        );
    }, [searchTerm, epikuretsOfType]);

    useEffect(() => {
        filterEpikurets();
    }, [selectedType]);

    const updateType = useCallback((index: number) => {
        setSelectedType(index);
    }, []);

    const buttons = epikuretTypes.map((type) => {
        const buttonLabels = ui.screens.texts.buttonLabels as unknown as {
            [key: string]: string;
        };
        return buttonLabels[type] || '!No translation!';
    });

    console.log(
        11.1,
        matchingEpikurets.map((e) => e.epikuretId),
        epikuretsOfType.map((e) => e.epikuretId)
    );
    const epikuretElements = useMemo(
        () =>
            matchingEpikurets
                .reduce((acc: EpikuretDate[][], matchingEpikuret: EpikuretDate) => {
                    const arrayOfSameType = acc.find(
                        (epikurets) => epikurets[0].epikuretId === matchingEpikuret.epikuretId
                    );
                    if (arrayOfSameType) {
                        arrayOfSameType.push(matchingEpikuret);
                    } else {
                        acc.push([matchingEpikuret]);
                    }

                    return acc;
                }, [])
                .sort((a, b) => (a[0].year > b[0].year ? -1 : 1))
                .map((matchingEpikurets, index) => (
                    <Pressable
                        key={index}
                        onPress={() =>
                            navigation.navigate('Epikuret', {
                                epikurets: matchingEpikurets,
                                details: true
                            })
                        }
                    >
                        <Epikuret epikurets={matchingEpikurets} details={false} />
                    </Pressable>
                )),
        [matchingEpikurets]
    );

    return (
        <>
            <SearchBar
                style={styles.searchBar}
                placeholder={ui.screens.texts.placeholder}
                onChangeText={updateSearch}
                value={searchTerm}
            />
            <ButtonGroup
                onPress={updateType}
                selectedIndex={selectedType}
                buttons={buttons}
                containerStyle={styles.container}
                buttonStyle={styles.buttonGroupButton}
                selectedButtonStyle={styles.buttonGroupSelectedButton}
                innerBorderStyle={styles.buttonGroupBorder}
                selectedTextStyle={styles.buttonGroupSelectedText}
                textStyle={styles.buttonGroupText}
            />
            <ScrollView style={styles.container}>{epikuretElements}</ScrollView>
        </>
    );
}

const commonStyles: { [key: string]: FlexStyle & ViewStyle } = {
    searchBar: {},
    buttonGroupButton: {},
    buttonGroupSelectedButton: {},
    buttonGroupSelectedText: {},
    buttonGroupText: {},
    buttonGroupBorder: {},
    container: {
        height: 24,
        backgroundColor: 'transparent',
        borderWidth: 0,
        display: 'flex'
    }
};
