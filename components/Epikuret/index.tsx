import React from 'react';
import { ViewStyle, TextStyle } from 'react-native';
import { Card } from 'react-native-elements';
import _ from 'lodash';

import useStyles from '../../hooks/useStyles';
import { EpikuretDate } from '../../types';
import EpicurusEpikuret from './EpicurusEpikuret';
import ThoughtEpikuret from './ThoughtEpikuret';
import BookEpikuret from './BookEpikuret';
import GlossaryEpikuret from './GlossaryEpikuret';

type Props = {
    epikurets?: EpikuretDate[];
    details?: boolean;
};

export const epikuretTypes = ['thought', 'epicurus', 'book', 'glossary'];

export default function Epikuret({ epikurets, details = false }: Props): JSX.Element {
    const { type } = (epikurets && epikurets[0]) || { type: 'none' };
    const styles = useStyles(commonStyles);

    return type === 'epicurus' ? (
        <EpicurusEpikuret epikurets={epikurets} details={details} />
    ) : type === 'thought' ? (
        <ThoughtEpikuret epikurets={epikurets} details={details} />
    ) : type === 'book' ? (
        <BookEpikuret epikurets={epikurets} details={details} />
    ) : type === 'glossary' ? (
        <GlossaryEpikuret epikurets={epikurets} details={details} />
    ) : (
        <Card containerStyle={styles.epikuretEpicurusCard}>
            <Card.Title style={styles.epikuretEpicurusTitle}>No Epikuret found.</Card.Title>
        </Card>
    );
}

const commonStyles: { [key: string]: ViewStyle & TextStyle } = {
    epikuretEpicurusCard: {
        marginTop: 15,
        width: '95%'
    },
    epikuretEpicurusTitle: {
        fontWeight: 'normal',
        fontFamily: 'TheGirlNextDoor_Regular',
        textAlign: 'center',
        paddingTop: 20
    }
};
