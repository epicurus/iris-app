import React, { useState, useEffect } from 'react';
import { ScrollView, StatusBar, FlexStyle, TextStyle, Pressable } from 'react-native';
import { StackScreenProps } from '@react-navigation/stack';
import _ from 'lodash';
import Constants from 'expo-constants';
import useStyles from '../../hooks/useStyles';
import useGoogleCalendar, { EventDate } from '../../hooks/useGoogleCalendar';
import Event from '../../components/Event';
import { TopTabParamList, RootStackParamList } from '../../types';

type Props = StackScreenProps<TopTabParamList & RootStackParamList, 'Home'>;

const googleCalendarIcalURl = Constants.manifest.extra.googleCalendarIcalUrl;

export default function TabEventsScreen({ navigation }: Props): JSX.Element {
    const styles = useStyles(commonStyles);
    const [displayedEvents, setDisplayedEvents] = useState<EventDate[]>([]);
    const events = useGoogleCalendar(googleCalendarIcalURl);

    useEffect(() => {
        if (Array.isArray(events)) {
            setDisplayedEvents(events);
        } else if (events instanceof Error) {
            console.error(Error);
        }
    }, [events]);

    return (
        <ScrollView style={styles.container}>
            {events &&
                displayedEvents.map((event: EventDate, index: number) => (
                    <Pressable onPress={() => navigation.navigate('Event', { event })} key={index}>
                        <Event event={event} />
                    </Pressable>
                ))}
        </ScrollView>
    );
}

const commonStyles: { [key: string]: FlexStyle & TextStyle } = {
    container: {
        flex: 1,
        paddingTop: StatusBar.currentHeight
    },
    text: {
        fontSize: 24
    }
};
