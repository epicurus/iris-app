import epikuretImage0 from '../images/2bbfe9ebef6074125834cb0e4a91aff3b700d94d.jpg';
import epikuretImage1 from '../images/5bb62d6c8fb559471ad6b7f778a3711cf1d517bf.jpg';
import epikuretImage2 from '../images/aadbcafba46e94a4ab184c142644a0540a09acba.jpg';
import epikuretImage3 from '../images/30b27002ea9821fe4a902fa8afbc9084583aacd3.jpg';
import epikuretImage4 from '../images/a36376dd700419546de7e37c49338a15d334b736.jpg';
export default [
    {
        epikuretId: '979-9197549835',
        title: 'Anregungen zum Gespräch ... Anpassung',
        type: 'book',
        year: '2008',
        promotion: 'none',
        book: '9197549835',
        cover: epikuretImage0,
        download:
            'https://storage.googleapis.com/ewer/_home_michael_Der-Garten-des-Epikur_ebooks_iris-anpassung_public_Miteinandersprechen...überAnpassung_de.rtf',
        subtitle: 'Buch, 2008',
        thought: [
            'Iris Johansson über Anpassung. Die ersten deutschen Übersetzungen. In der Übersetzung von Marcel Desax.'
        ]
    },
    {
        epikuretId: '979-9197549843',
        title: 'Anregungen zum Gespräch ... Ausgebranntsein',
        type: 'book',
        year: '2008',
        promotion: 'none',
        book: '979-9197549843',
        audiobook: '4061707442063',
        ebook: 'B08ZNDN5DG',
        cover: epikuretImage1,
        download:
            'https://storage.googleapis.com/ewer/_home_michael_Der-Garten-des-Epikur_ebooks_iris-ausgebranntsein_public_Miteinandersprechen...überAnpassung_de.rtf',
        subtitle: 'Buch, E-Buch, Hörbuch, 2008',
        thought: [
            'Iris über Ausgebranntsein. Die ersten deutschen Übersetzungen. In der Übersetzung von Marcel Desax.'
        ]
    },
    {
        epikuretId: '978-3825177911',
        title: 'Eine andere Kindheit: Mein Weg aus dem Autismus',
        type: 'book',
        year: '2019',
        promotion: 'none',
        book: '978-3825177911',
        ebook: 'B015WRY8Q2',
        cover: epikuretImage2,
        buy: {
            book: [
                {
                    provider: 'Amazon',
                    url: 'https://www.amazon.de/Eine-andere-Kindheit-Mein-Autismus/dp/3825177912'
                }
            ],
            ebook: [
                {
                    provider: 'Amazon',
                    url: 'https://www.amazon.de/Eine-andere-Kindheit-Mein-Autismus-ebook/dp/B015WRY8Q2'
                }
            ]
        },
        subtitle: 'Buch, E-Buch, 2019',
        thought: [
            'Von ihrer Umgebung unverstanden und als "geistig behindert" angesehen, lebt die kleine Iris in ihrer ganz eigenen Welt. Sie kann kaum kommunizieren und sitzt oft stundenlang mit schaukelndem Oberkörper in einer Ecke. Nur ihr Vater sieht, dass Iris nicht behindert, sondern einfach anders ist. Mit viel Liebe und Einfühlungsvermögen hilft er seiner autistischen Tochter, aus ihrer "Richtigen Welt" hinein in die "Normale Welt" zu finden.',
            'Heute ist Iris Johansson Expertin für Kommunikation und kann ihre Erlebnisse endlich in Worte fassen. In ihrer ganz eigenen ausdrucksstarken Sprache eröffnet sie dem Leser eine Welt von unglaublichen geistigen und seelischen Dimensionen. Sie berichtet von Außerkörperlichen Erfahrungen und von ihren Erkenntnissen über das Entstehen von Kommunikation und menschlichen Beziehungen.'
        ]
    },
    {
        epikuretId: '978-3751936439',
        title: 'Gemeinsam im Gespräch ... über Irrgefühl',
        type: 'book',
        year: '2020',
        promotion: 'none',
        book: '978-3751936439',
        audiobook: '4061707490859',
        ebook: 'B08L543Q8X',
        cover: epikuretImage3,
        download:
            'https://storage.googleapis.com/ewer/_home_michael_Der-Garten-des-Epikur_ebooks_iris-irrgefuehl_public_GemeinsamimGespräch...überIrrgefühl_de.rtf',
        buy: {
            book: [
                {
                    provider: 'Amazon',
                    url: 'https://www.amazon.de/Gemeinsam-Gespr%C3%A4ch-%C3%BCber-Irrgef%C3%BChl-Iris/dp/3751936432'
                },
                {
                    provider: 'BoD Buchshop',
                    url: 'https://www.bod.de/buchshop/gemeinsam-im-gespraech-ueber-irrgefuehl-iris-johansson-9783751936439'
                }
            ],
            ebook: [
                {
                    provider: 'Amazon',
                    url: 'https://www.amazon.de/Gemeinsam-Gespr%C3%A4ch-%C3%BCber-Irrgef%C3%BChl-Iris-ebook/dp/B08L543Q8X'
                }
            ],
            audiobook: [
                {
                    provider: 'Amazon',
                    url: 'https://www.amazon.de/Gemeinsam-Gespr%C3%A4ch-%C3%BCber-Irrgef%C3%BChl-Iris/dp/B08XQWFSRJ'
                },
                {
                    provider: 'Audible',
                    url: 'https://www.audible.de/pd/Gemeinsam-im-Gespraech-ueber-Irrgefuehl-Hoerbuch/B08XQT81YC'
                }
            ]
        },
        subtitle: 'Buch, E-Buch, Hörbuch, 2020',
        thought: [
            'Ich fühle mich bedrückt, unleidig, vom Leben ungerecht behandelt, habe Kopfschmerzen, will am liebsten morgens im Bett bleiben, schaue mit Argwohn auf Menschen um mich herum, werde unfair ... Es geht mir nicht so toll.',
            'Iris hat über 60 Jahre ihres Lebens in ihrer ureigenen autistischen Akribie, damit verbracht, Phänomene wie Irrgefühl (im Schwedischen Ångest) zu erforschen, zu verstehen und auf eine Weise zu beschreiben, dass Menschen frei sind, ihre Gedanken zu nehmen oder zu lassen. Meist in Gesprächsrunden und Einzelgesprächen.',
            'Ein solches Gespräch ist die Grundlage dieses Buches.'
        ]
    },
    {
        epikuretId: '979-8683069964',
        title: 'Gemeinsam im Gespräch ... über Sexualität',
        type: 'book',
        year: '2021',
        promotion: 'none',
        book: '979-8683069964',
        audiobook: '4061707442063',
        ebook: 'B08ZNDN5DG',
        cover: epikuretImage4,
        download:
            'https://storage.googleapis.com/ewer/_home_michael_Der-Garten-des-Epikur_ebooks_iris-sexualitaet_public_GemeinsamimGespräch...überSexualität_de.rtf',
        buy: {
            book: [
                {
                    provider: 'Amazon',
                    url: 'https://www.amazon.de/Gemeinsam-im-Gespr%C3%A4ch-%C3%BCber-Sexualit%C3%A4t/dp/B08ZG1Y47Q'
                }
            ],
            ebook: [
                {
                    provider: 'Amazon',
                    url: 'https://www.amazon.de/Gemeinsam-im-Gespr%C3%A4ch-%C3%BCber-Sexualit%C3%A4t/dp/B08ZG1Y47Q'
                }
            ],
            audiobook: [
                {
                    provider: 'Amazon',
                    url: 'https://www.amazon.de/Gemeinsam-im-Gespr%C3%A4ch-%C3%BCber-Sexualit%C3%A4t/dp/B08ZNT6XH4'
                }
            ]
        },
        subtitle: 'Buch, E-Buch, Hörbuch, 2021',
        thought: [
            'Was Iris über Sexualität sagt, ist nicht spektakulär, eher unscheinbar, wie „Man kann Sexualität nicht machen, man kann nur in sie hineingehen“.',
            'Aber wenn man nicht darüber hinweggeht, dann können sie eine Kraft entfalten und wirklich etwas ändern.',
            'Meine sexuellen Erlebnisse wurde verblüffend anders, freier, mit einem neuen Atem, leichter und einem Lachen am Ende.'
        ]
    },
    {
        epikuretId: '02-Reading-the-athmosphere-in-zoom',
        title: 'Über den Unterschied zwischen Zoom-Treffen und physischen Treffen',
        type: 'epicurus',
        audio: 'https://storage.googleapis.com/ewer/_home_michael_Der-Garten-des-Epikur_storage_Garden-Of-Epicurus_2021-02-23_02-Reading-the-athmosphere-in-zoom_de-DE.mp3',
        promotion: 'home',
        subtitle: 'Der Garten des Epikur, 23. Feb 2021',
        dialog: [
            'Iris, bei der Einführung hast du gesagt, dass du dann auch ab und zu in die in die Atmosphäre schaust und da drin liest, was du da siehst. Wie funktioniert das über Zoom?',
            'Iris: Es ist genauso wie wenn wir mit dem Abstand von einem Meter miteinander da sind, oder ob jemand in Australien ist und jemand ist in Schweden, bei der Atmosphäre gibt es keinen Abstand. Es ist nur hier und jetzt und anwesend. In dem Dunklen des Universums gibt es keine Distanz, da gibt es keine Zeit und keinen Raum. Es gibt nur hier und jetzt und Anwesenheit. Alle Informationen aus der Kindheit der Menschheit bis hin zum Untergang der Menschheit gibt es da in der Atmosphäre. Es gibt vielleicht auch Informationen von anderen Planeten, aber da ich diese Sprache nicht spreche, da weiß ich nichts davon. Ja, ich nenne dies Sehen, aber es ist eigentlich ein Wissen davon. Ich nenne das einfach Sehen, damit es nicht so ein ein Hokuspokus zu sein scheint.'
        ]
    },
    {
        epikuretId: '12-Energy to work',
        title: 'Tausendmal mehr Kraft, als wir brauchen',
        type: 'epicurus',
        audio: 'https://storage.googleapis.com/ewer/_home_michael_Der-Garten-des-Epikur_storage_Garden-Of-Epicurus_2021-02-23_12-Energy to work_de-DE.mp3',
        promotion: 'home',
        subtitle: 'Der Garten des Epikur, 23. Feb 2021',
        dialog: [
            'Mir begegnet öfter das Phänomen, dass mir Leute erzählen, dass das früher die Leute irgendwie mehr Energie hatten, also den ganzen Tag durcharbeiten konnten und dass das heute irgendwie weniger ist. Und ich habe mich gefragt, hängt das vielleicht mit der dritten und der vierten Kommunikationsdimension zusammen, dass da irgendwie etwas nicht mehr so richtig passt, oder was könnte das sein?',
            'Iris: Das hängt mit unserer Vorstellung und mit unserer Anpassung weitgehend zusammen. Z.b. wenn du wenn du jetzt von Kraft sprichst oder von Energie, dann dann sprichst du davon wie, wenn eine bestimmte Menge davon gäbe und das ist ein Majoritätsmissverständnis. In dem Augenblick, wo du Angst bekommst, dass deine Energie nicht ausreichen wird, bekommst du Angst und da da fehlt dir der Zugang zur Kraft. Wenn du darauf vertraust, dass du tausendmal mehr Kraft hast als du brauchst, da ist die Energie in ständiger Bewegung. Sobald du Angst bekommst, dass du nicht ausreichend Energie haben wirst, da bekommst du Angst und da lässt die Kraft sich hängen und das ist der Grund weshalb Menschen erlahmen oder keine Kraft mehr haben. Also wenn wir in diesem Denkfehler drin sind, da kommt dieses Nachlassen der Kraft früher und früher und da fehlt uns der Zugang zu der Kraft. So, das ist der Grund. Wenn wir dann alles Notwendige Unbequeme sofort machen, da wird der Körper froh und zufrieden. Das ist in etwa wie wenn einem kalt ist und man zieht etwas an, aber man bleibt so still wie möglich sitzen oder liegen, damit man möglichst viel Kraft sparen kann, damit man sich wärmen kann quasi. Dann wird man nicht warm. Wenn man aber anfängt zu tanzen oder Gitarre zu spielen und zu singen oder hin und her läuft und anschließend dann da sich einhüllt da bleibt die Wärme ei einem und unter Umständen bleibt einem warm die ganze Nacht hindurch. Wenn man das entdeckt hat, dann wird man sehen, dass man so einfach hindurch arbeiten kann ohne Probleme. Und dann spricht Iris von ihrer eigenen Arbeit. Sie kann so 20 Stunden am Stück aktiv sein und arbeiten, aber dann sagt sie, davon reden wir jetzt nicht, das ist ja Arbeit-Narkomanie, das ist zu viel auf der anderen Seite und nicht zu wenig auf der einen Seite. Man hat ja so seine Fehler und Mängel. Gleichgewicht oder Ausgewogenheit, auf die kommt es an.'
        ]
    },
    {
        epikuretId: '01-indifference',
        title: 'Gleichgültigkeit ist das andere Ende der Liebe',
        type: 'epicurus',
        audio: 'https://storage.googleapis.com/ewer/_home_michael_Der-Garten-des-Epikur_storage_Garden-Of-Epicurus_2021-03-23_01-indifference_de-DE.mp3',
        promotion: 'home',
        subtitle: 'Der Garten des Epikur, 23. Mär 2021',
        dialog: [
            'Ja, ich beschäftige mich in der letzten Zeit viel mit der Gleichgültigkeit als Phänomen. Also, es geht mir darum, dass ich beobachte, dass ich und auch andere Menschen, bei ziemlich bedeutsamen Sachen einfach kalt bleibe. Es fällt mir natürlich leichter, das bei anderen zu sehen. Und da habe ich ein Beispiel: In deinem Irrgefühl-Buch schreibst du, dass die meisten Krankheiten von Irrgefühlen kommen und dann habe ich das öfter mal erzählt, erwähnt und immer gemerkt, ja, das geht so vorbei, auch bei Menschen, die sehr, sehr viel von dir halten. Und da hätte ich gerne etwas davon gehört, wie das ist, dass das auch Dinge, wo die meisten Krankheiten herkommen, einfach so vorbeifließen.',
            'Iris: Die Liebe ist ja ein Zustand, der die ganze Zeit da ist in der Atmosphäre. Und es ist ziemlich leicht, diesen Zustand zur Liebe heraufzuziehen, indem einer meiner Sinne amüsiert wird, an dem sich amüsiert. Der Zustand Liebe, wenn man diesen zum Gefühl heraufzieht, man wird da offen und verletzlich, und es entstehen lauter verschiedene Empfindungen, Gefühle in einem selber. Wenn die Liebe dann umfällt auf die eine oder andere Weise, dann wird sie zum Hass und das ist das andere Ende, die andere Seite von dem gleichen Steg, von dem gleichen Ast. Also all diese Empfindungen, all diese Gefühle, die Liebe bis hin zum Hass, es ist die Lebenskraft, die all diese Gefühle erschafft. Andererseits, die andere Seite davon ist die Gleichgültigkeit. Wenn etwas im Weg steht diesem Zustand und diesen Zustand der Liebe blockiert, wenn die Liebe nicht sozusagen über uns herfallen darf, dann dann entsteht Gleichgültigkeit.'
        ]
    },
    {
        epikuretId: '04-advertizment-and-tiredness',
        title: 'Müde werden wegen Werbung',
        type: 'epicurus',
        audio: 'https://storage.googleapis.com/ewer/_home_michael_Der-Garten-des-Epikur_storage_Garden-Of-Epicurus_2021-03-23_04-advertizment-and-tiredness_de-DE.mp3',
        promotion: 'home',
        subtitle: 'Der Garten des Epikur, 23. Mär 2021',
        dialog: [
            'Und die andere Frage ist, dass ich auch oft merke, dass ich sehr müde bin, wie soll ich sagen, wenn ich in der Stadt rumlaufe und so Werbungen sehe und Sachen und so und dann irgendwann kommt so eine Art Müdigkeit, so ach mein Gott, ich finde, dass es zuviel ist. Und in dieser Kaufgesellschaft, die wir sind, also dass ich immer so ein Gefühl bekomme, ich müsste jetzt, keine Ahnung, anders aussehen oder mich mehr um stilvolle Kleidung oder um Haushalt kümmern und ich kaufe nix, aber einfach das sogar anzugucken macht mich sehr müde. Ob sie über diese Müdigkeit etwas sagen kann, also warum werde ich müde, wenn ich viele Sachen sehe?',
            'Iris: Das, worauf du reagierst, das, was dich müde macht, das ist, wenn du in der Stadt unterwegs bist, dann siehst du die Farben, du siehst die Bewegungen, du hörst die Geräusche und diese Werbung ist so gemacht, dass wir uns nicht gegen sie wehren können. Wir müssen sie aufnehmen. Die Werbung ist so schlau angefertigt, dass sie hineingeht, und es ist die Absicht, dass du durch die Werbung auf einer bewussten Ebene entweder das magst oder das nicht magst. Du gerätst in einen Affekt dadurch. Die Absicht ist, dass es wie eine Tätowierung auf der Innenseite werden soll. Also, wenn du in einen Laden kommst dann und du siehst dieses Warenzeichen, womit du tätowiert geworden bist, dann ist es so gemeint, dass du dich hingezogen fühlen sollst zu diesem Warenzeichen und dass du das erwerben sollst. Es ist es ist gewissermaßen die Absicht, dass wir angepasst werden sollen an das. Und bei aller Anpassung entsteht eine Art von Müdigkeit, eine Art von Trauer.'
        ]
    },
    {
        epikuretId: '14-World-situation-changed-since-covid',
        title: 'Über die Gefährung der Demokratie',
        type: 'epicurus',
        audio: 'https://storage.googleapis.com/ewer/_home_michael_Der-Garten-des-Epikur_storage_Garden-Of-Epicurus_2021-03-23_14-World-situation-changed-since-covid_de-DE.mp3',
        promotion: 'home',
        subtitle: 'Der Garten des Epikur, 23. Mär 2021',
        dialog: [
            'Ja, die Frage geht um die Weltlage und wie Iris darüber denkt, wie wir uns zu verhalten können. Seit Anfang der Corona-Pandemie, denke ich, wird mir immer klarer, dass irgendwas, was vermittelt wird im Großen mehr und mehr nicht mit der Wirklichkeit übereinstimmt. Dann Samstag war ich zum ersten Mal hier in Stockum bei einer Demonstration dabei und ich ging dahin im Namen der Demokratie, die die sogar in Schweden so leichtfetig, ziemlich einfach jetzt geschädigt wurde, durch ein Gesetz, wo sich höchstens acht Personen versammeln dürfen. Ja, ich wurde dabei gefilmt und nach ein paar Stunden hatten 40 oder 500 Menschen auf Facebook dann geschrieben was für eine Irrer ich bin, dass ich andere Menschen in ihrem Leben bedrohe, wegen meinem Dasein. Und das weckt viele Fragen.',
            'Iris: Ich habe heute beinahe ein Heft zum sekundären Leben fertig redigiert. Und in diesem Heft steht eine ausführliche Antwort auf deine Frage. Der Titel lautet: Gemeinsam im Gespräch ... über sekundäres Leben. Aber ich werde trotzdem dir jetzt eine Antwort geben, weil die Frage eine sehr interessante Frage ist. Wenn wir von der Wirklichkeit reden, dann verstehen wir darunter all das, was man zum Gegenstand sich nehmen kann, was man zu einer Sache machen kann. Bei diesem haben wir ein Nullsummenspiel geschaffen. Wir haben eine Ökonomisierung dessen geschaffen. Und im Gegensatz zum Primären ist dies sekundär, elementar und wählbar. Die Menschen können hierüber verfügen, wir können dieses lenken. Dadurch, dass dies ein Spiel ist und es gibt Spielregeln, gibt es Gewinner und Verlierer. Wenn jemand gewinnt, gibt es jemanden anders, der verliert. Und ich glaube, dass es dieses Spiel ist, was du ahnst. Und dieses Spielen, ganz richtig, gefährdet auch die Demokratie, wie du diese Ahnung hast. Es baut auf ein unmenschliches Denken und ich glaube, es ist dieses, was in dir ein Ungeräusch hervorbringt. Um dieses zu verstehen, braucht man zu wissen, dass das unser Bewusstsein im Grunde genommen ein Schutz- und Verteidigungsmechanismus ist zum Überleben. Und uns ist diese Voraussetzung gegeben worden, denn in der wilden Natur haben wir sonst wenige Voraussetzungen zum überlegen. Unsere Sinne sind nicht scharf genug, wir sehen nicht genug, wir hören nicht genug, wir spüren die Düfte nicht genau genug, um in der wilden Natur klarzukommen. Wir brauchen die Hilfe von anderen ziemlich lange Zeit bis bis wir soweit sind, dass wir da etwas unternehmen können und dass wir selber etwas tun können. Und auch wenn wir dann herangewachsen sind, rennen wir trotzdem zu langsam, oder wir streiten nicht gut und wir fallen wilden Tieren leicht dann zum Opfer. Und daher haben wir einen ein Bewusstsein bekommen und ein Sprachorgan bekommen, damit wir uns mit anderen austauschen können und wir brauchen Teil einer Herde zu sein. Was ist es denn, was das Menschliche kennzeichnet. Wenn man in einer Gruppe ist mit anderen in der wilden Natur, da schaut man darauf, dass niemand daran verliert.',
            'Iris: Es ist das, was Urvölker kennzeichnet oder auszeichnet, es ist dass die Menschen in der Gruppe sich so umeinander kümmern, dass niemand verliert. Es ist dieses, was eine nachhaltige Herde erschafft für Menschen. Wenn wir dann die Gesellschaft so organisiert haben, dass es keine äußeren Gefahren gibt, und wir benutzten unser Bewusstsein dazu, unseren Intellekt zu betätigen und wir machen Gebrauch von unserem Bewusstsein. Wir brauchen ja nicht mehr dieses Bewusstsein, um in der Herde zu leben, aber wir stattdessen machen wir dann Gebrauch von unserem Bewusstsein auf eine unmenschliche Weise. Und ich glaube, dass es dies ist, was so in dir quietscht. Ein System, das ökonomisiert wurde, ist ein ein unmenschliches System, und es wird zum gefühlsmäßigen Spiel. Und das Schlimmste der gefühlsmäßigen Spiele, die stattfinden in unserer Welt, das sind die Kriege. Und dieses Spiel besteht darin, einen einen Menschen soweit Angst einzujagen, dass er unmenschlich wird, ohne dass diese Mensch weiß, dass er Angst hat. Darin besteht die Radikalisierung. Ich glaube dass es dieses ist, was so sich in dir reibt, auch wenn du das so nicht in Worte gefasst hast, aber du kannst es dir ja überlegen.',
            'Du meinst, dass ich auf eine Art radikalisiert werde.',
            'Iris: Nicht, dass du radikalisiert wirst, dass du dadurch bedroht wirst durch die Radikalisierung, die in der Gesellschaft stattfindet. Leider hast du auch darin recht, dass wir alle radikalisiert werden, wenn wir Angst bekommen. Also, wir sind auch nicht davon frei.'
        ]
    },
    {
        epikuretId: 'Glossary/the-primary',
        title: 'Das Primäre',
        author: 'Dieter',
        type: 'glossary',
        video: 'https://storage.googleapis.com/ewer/_home_michael_Der-Garten-des-Epikur_apps_source_epikurets_Glossary_the-primary_de-DE-`Das Primäre`[Dieter].mp4',
        promotion: 'none',
        dialog: []
    },
    {
        epikuretId: 'Glossary/the-primary',
        title: 'Das Primäre',
        author: 'Eelke',
        type: 'glossary',
        video: 'https://storage.googleapis.com/ewer/_home_michael_Der-Garten-des-Epikur_apps_source_epikurets_Glossary_the-primary_de-DE-`Das Primäre`[Eelke].mp4',
        promotion: 'none',
        dialog: []
    },
    {
        epikuretId: 'Glossary/the-primary',
        title: 'Das Primäre',
        author: 'Felice',
        type: 'glossary',
        video: 'https://storage.googleapis.com/ewer/_home_michael_Der-Garten-des-Epikur_apps_source_epikurets_Glossary_the-primary_de-DE-`Das Primäre`[Felice].mp4',
        promotion: 'none',
        dialog: []
    },
    {
        epikuretId: 'Glossary/the-primary',
        title: 'Das Primäre',
        author: 'Filip',
        type: 'glossary',
        video: 'https://storage.googleapis.com/ewer/_home_michael_Der-Garten-des-Epikur_apps_source_epikurets_Glossary_the-primary_de-DE-`Das Primäre`[Filip].mp4',
        promotion: 'none',
        dialog: []
    },
    {
        epikuretId: 'Glossary/the-primary',
        title: 'Das Primäre',
        author: 'Lukas',
        type: 'glossary',
        audio: 'https://storage.googleapis.com/ewer/_home_michael_Der-Garten-des-Epikur_apps_source_epikurets_Glossary_the-primary_de-DE-`Das Primäre`[Lukas].mp3',
        video: 'https://storage.googleapis.com/ewer/_home_michael_Der-Garten-des-Epikur_apps_source_epikurets_Glossary_the-primary_de-DE-`Das Primäre`[Lukas].mp4',
        promotion: 'none',
        dialog: [
            '(???)das primäre das was da ist wenn nicht andere Stahl das was man nicht verändern kann das mit dem man schließen kann das wohl keine Entscheidung möglich ist kann nur mit riesen das nicht da wenn alle alles was sonst blockiert und ein wenig mehr da'
        ]
    },
    {
        epikuretId: 'Glossary/the-secondary',
        title: 'Das Sekundäre',
        author: 'Michael',
        type: 'glossary',
        video: 'https://storage.googleapis.com/ewer/_home_michael_Der-Garten-des-Epikur_apps_source_epikurets_Glossary_the-secondary_de-DE-`Das Sekundäre`[michael].mp4',
        promotion: 'none',
        dialog: []
    },
    {
        epikuretId: 'epicurus-introduction-2021',
        title: 'Der Garten des Epikur - virtuell',
        type: 'thought',
        origin: 'iris',
        year: '2021',
        promotion: 'none',
        subtitle: 'Iris, 2021',
        thought: [
            'Epikur war ein Philosoph, der einige hundert Jahre vor Christus in Griechenland lebte. Er gehörte der Oberschicht an, hatte aber eine völlig andere Lebenseinstellung als andere Philosophen seiner Zeit. Er versammelte Menschen, die sich für seinen großen Park interessierten, setzte sich zu ihnen und sprach über das Leben und den Sinn des Lebens im Leben, und über das Gleichgewicht in sich selbst, um ein zufriedenes Leben zu führen, und das, was er als "Wohlfühlen" bezeichnete. Ihm wurde vorgeworfen, dass dies Hedonismus sei, ein ungezügelter Wunsch nach Vergnügen, wo es darum ging, die berauschende Wirkung von Kicks zu optimieren und zu messen, die man emotional, vorzugsweise erotisch oder durch aufgeregte Gewalt bekommen konnte. Das war etwas völlig anderes als Epikurs ausgleichendes Denken. Diese Sichtweise wurde geschaffen, um die epikuräische Art der Welt zu begegnen zu brechen, und das harmonische Gleichgewicht in der Begegnung und im Zusammensein und das friedliche, gegenseitige Heilen, was daraus entsprang: ein gesundes, einfaches Leben.\\nEpikur selbst hatte schweres Rheuma, so dass seine Schüler ihn jeden Morgen mit in den Garten nahmen und in die Sonne legten. Als er genügend aufgewärmt war, wurde er auf eine Streckbank gespannt und auseinandergezogen. Dort prägte er einen seiner bekannten Sätze: "Sogar auf der Streckbank kann man eine Unze Glück erleben." Wenn der der Zahnradmechanismus beim Auseinanderziehen wieder einwenig losließ, die Streckung nachließ, gab es eine kurze Zeit der Schmerzfreiheit. Die fühlte er als einen Moment reinen Glücks.\\nAls ich Philosophie studierte, fand ich ihn und er war für alles verantwortlich, was ich mir ausgedacht hatte. Ich bin sein "Waffenträger". Ich trete in seine Fußstapfen und kümmere mich um diejenigen, die mir über den Weg laufen, indem ich seiner Tradition folge und seinen Garten pflege. Im Moment international "online". Also ... wenn Du in meinen Garten des Epikur kommst, kannst Du dir überlegen, mit welchen Fragen Du dich im Inneren beschäftigst, über welchen Dinge Du nicht glaubst sprechen zu können oder neues darüber zu erfahren ... privat, persönlich, philosophisch usw., auf das Du selbst keine Antwort hast, aber gerne darüber nachdenken möchtest. Ich nehme alle Fragen ernst und manchmal sage ich natürlich, ich weiß es nicht, aber oft treten Informationen in der Atmosphäre hervor, die ich erfassen und vermitteln kann. Das ist Philosophieren und es ist so interessant und du bist willkommen. Du kannst auch nur zum Zuhören kommen und dir dort vielleicht etwas einfallen lassen, worüber Du fragen kannst, oder auch nicht. Dein Zuhören wirkt in die Atmosphäre, die auf die gleiche Weise bildet und bereichert wie Fragen ... Fühl Dich frei zu kommen, nur weil Du neugierig bist. Manchmal ist es auch eine Primärarbeit, Epikurs Arbeitsweise mit Respekt und Wertschätzung zu reflektieren, in Selbstvertrauen, in der Gemeinschaft mit anderen und in der inneren Sicherheit zu sein. Dieser Philosoph nahm viel Geld von denen, die es hatten und ließ Frauen und Sklaven kostenlos dabei sein, und manchmal bekamen sie eine Mahlzeit und ein Kleidungsstück dazu. Ich mag es, diese Tradition aufrechtzuerhalten, über andere, die das koordinieren, das sind Michael und Benedicte in Deutschland und Koordinator Filip Wall in Schweden.'
        ]
    },
    {
        epikuretId: 'thoughts-primary-reference',
        title: 'Primärer Bezugspunkt',
        type: 'thought',
        origin: 'ksk',
        year: '2013',
        promotion: 'none',
        subtitle: 'Kommunikation als Kunst, 2013',
        thought: [
            'REFERENZPUNKT, einige Beispiele, sich selbst darin zu üben.',
            'Wenn du du selbst bist wie du selbst bist und vor dem Spiegel stehst und dein Bild im Spiegel ist dein „Publikum“, mit dem du sprichst und du schaust und fühlst nach , wie du auf das reagierst, was du zu dir sagst.',
            'Beginne damit, zu denken:',
            'Das Leben ist der Sinn des Lebens und ich lebe und bin hier und jetzt vor diesem Spiegel und ich fühle mich etwas... und da seh ich soo aus.',
            'Der Wert des Lebens liegt darin, dass es mich gibt und ich bin... Ich kann darin ruhen, in mir selbst, kann mich o.k. fühlen – damit – Geht nicht, etwas dazu zu legen oder etwas wegzunehmen, es ist –',
            'In Gemeinschaft bin ich – mit mir selbst, wenn ich keine Angst habe – und mich nicht selbst bewerte – weder nach oben noch nach unten, nur sein –',
            'In Geborgenheit bin ich, Grundzustand, wenn ich weiß, dass mein Leben nicht bedroht ist – und wenn ich nicht glaube, dass ich etwas von außen bekommen muss – um darin zu sein. Ich bin erwachsen, mehr als 20 Jahre, und da bin ich ausgewachsen – und kann innen drin daran anknüpfen, dass ich fähig bin, mich um mich selbst zu kümmern – unabhängig davon ob etwas von außen mir Angst macht – bin hier und jetzt – unbedroht –',
            'Lass den Gedanken los, richtig zu machen – oder tüchtig zu sein – wende es um. Es ist richtig und du bist tüchtig, wenn du nichts tust – und wenn du das alltäglich Notwendige tust ohne es zu bewerten als schwer oder bedauerlich – ohne zu bewerten – wenn du dann etwas Schlechtes entdeckst oder etwas was du falsch findest, so setze den Fokus darauf – und dann sieh was du lernen kannst. Lerne, lerne, lerne und versöhne dich damit, dass so die Wirklichkeit ist. Lass deine Zufriedenheit von innen kommen, wenn du das lebst.',
            'Der nächste Schritt ist, die Fehler zu berichtigen, wenn es etwas zu berichtigen gibt. Einige berichtigen sich selbst nur schon durch das Bewusstsein und dass man sie nicht wiederholt – andere kann man nicht berichtigen und stattdessen braucht man sich damit zu begnügen, sich damit zu versöhnen – dass sie so wurden wie sie wurden – Einen Teil in der Wirklichkeit kann man ändern – Ansichten – Richtungen – Verständnis haben, mitsamt konkretem Berichtigen –',
            'Ständig zu wissen, dass Fehler passieren – weil das Leben Unvollkommenheit erfordert, damit es da sein kann. „Ich“ bin nicht der Ursprung davon, sondern bin unvollkommen und das gilt auch für „andere“, sie sind nicht der Ursprung des Bösen, sondern die Unvollkommenheit bringt es hervor.',
            'Der Körper produziert Gefühle, damit wir unsere Vorstellungen verändern können, die nicht länger stimmen in der Wirklichkeit. Wenn wir jemanden verlieren, werden wir traurig und weinen und trauern, bis wir entladen haben, was der Körper produziert hat und dann werden wir etwas wehmütig – Da war die Vorstellung, die wir hatten, dass es das gab, was es jetzt nicht mehr gibt – Wird möglich, weiter zu leben – Dass wir es begreifen und es so allmählich in Vergessenheit fällt.',
            'Intimität ist – sie waltet, wenn wir nur in uns selbst sind, im Gemeinsamen, hier und jetzt – in uns selbst – in Begegnung mit anderen – in Beziehung zu allem, was um uns ist – in dem zusammenhang, in dem wir uns befinden – usw. Da wird der Körper froh und wir holen Liebe hoch zum Gefühl – wir fühlen das Leben lebendig –',
            'Schmerz entsteht in allen Gefühlen – Schmerz ist des Körpers natürliche Weise zu kommunizieren. Das erhöht das Bewusstsein um die Unvollkommenheit des Lebens und gibt uns Raum, die Bedingungen des Lebens zu verstehen.',
            'Schmerz ist unser Verbündeter und wir sterben nicht davon – er steht im Dienst des Lebens.',
            'Der Autopilot, das Schutz- , Flucht- und Verteidigungssystem, das in unserem Körper tickt, wenn er glaubt, dass er in Lebensgefahr ist – tue nichts – halte ein – sieh dich um hier und jetzt – stelle fest, ob es lebensgefährlich ist – wenn nicht, nimm dir Zeit und werde dir bewusst, dass du Angst hast.',
            'Wenn Gefahr da ist, wirst du automatisch den Impulsen folgen, die vom Autopiloten kommen, und sie kommen auch, wenn es nicht relevant ist und dann braucht man inne zu halten – bewusst zu werden über die Situation hier und jetzt, die Ungefährlichkeit festzustellen, und – umzudenken –',
            'WENN DU DAS ÜBST, SO IST DAS DAS VORSTADIUM ZUR KOMMUNIKATION, WAS DU MACHST, IST, IN BEWUSSTHEIT ZU LANDEN UND DARIN ZU RUHEN, NICHT DENKEN, NUR SEIN – – – UND DANN KANNST DU FLOTT DAZU ÜBERGEHEN, MIT DIR SELBST IM SPIEGEL ZU SPRECHEN.',
            'Frage dich selbst,',
            'Wie denke ich über Liebe?',
            'Du wirst auf Wörter und Meinungen stoßen, die neue Fragen wecken, Phänomene, Verwunderungen, die du drehen und wenden wirst um weiterzumachen und zu vertiefen.',
            'Mache weiter mit noch ein paar Wörtern, über die du denken kannst und umgehen damit und mit dir selbst sprechen darüber und sieh, wie du darauf reagierst und welche Wertungen sie wecken.',
            'BEELTERUNG',
            'Das sind nur wenige Beispiele von Phänomenen, die zu gebrauchen sind zum Üben in der Kommunikation als Kunst. Sie sind nur dafür da, zu sein und einen kleinen Duft zu geben, worum es geht. Diese Übung wirst du die ganze Ausbildungszeit anwenden können und nach diesen 3 Jahren wirst du damit weitermachen, um die Lebenskunst zu vertiefen, das Primäre zu kultivieren und dich selbst zu fangen hier und jetzt in einer feinen Lebensqualität.',
            'WOHL BEKOMMS!',
            'Iris'
        ]
    }
];
