import React, { useState } from 'react';

import { ButtonGroup } from 'react-native-elements';
import { Ionicons } from '@expo/vector-icons';
import { Audio } from 'expo-av';

import useColors from '../hooks/useColors';

const BUTTON_SIZE = 64;

type Props = {
    audio: string;
};

type PlayMode = 'stopped' | 'paused' | 'playing';

const AudioPanel = ({ audio }: Props): JSX.Element => {
    const [sound, setSound] = useState<Audio.Sound>(new Audio.Sound());
    const [playMode, setPlayMode] = useState<PlayMode>('stopped');
    const [selectedAudioButton, setSelectedAudioButton] = useState<number>(0);
    const colors = useColors();

    async function playSound() {
        if (audio) {
            if (playMode === 'stopped') {
                const { sound } = await Audio.Sound.createAsync({ uri: audio });
                setSound(sound);

                await sound.playAsync();
            } else {
                await sound.playAsync();
            }

            setPlayMode('playing');
        }
    }

    async function stopSound() {
        if (sound) {
            sound.stopAsync();

            setPlayMode('stopped');
        }
    }

    async function pauseSound() {
        if (sound) {
            sound.pauseAsync();
            setPlayMode('paused');
        }
    }

    function onPressAudioButton(selectedIndex: number) {
        setSelectedAudioButton(selectedIndex);

        switch (playMode) {
            case 'stopped':
                playSound();
                break;
            case 'playing':
                if (selectedIndex === 0) pauseSound();
                else stopSound();
                break;
            case 'paused':
                if (selectedIndex === 0) playSound();
                else stopSound();
        }
    }

    React.useEffect(() => {
        return sound
            ? () => {
                  console.log('Unloading Sound');
                  sound.unloadAsync();
              }
            : undefined;
    }, [sound]);

    const availableButtons = [
        'md-play-circle-sharp',
        'md-pause-circle-sharp',
        'md-stop-circle-sharp'
    ].map((buttonType: string) => ({
        element: () => (
            <Ionicons name={buttonType as any} size={BUTTON_SIZE} color={colors.tintEpicurus} />
        )
    }));

    const displayedButtons =
        playMode === 'stopped'
            ? [availableButtons[0]]
            : playMode === 'playing'
            ? [availableButtons[1], availableButtons[2]]
            : [availableButtons[0], availableButtons[2]];

    return (
        <ButtonGroup
            onPress={onPressAudioButton}
            selectedIndex={selectedAudioButton}
            buttons={displayedButtons}
            containerStyle={{
                width: BUTTON_SIZE * 2.2,
                height: BUTTON_SIZE,
                backgroundColor: 'transparent',
                borderWidth: 0,
                display: 'flex'
            }}
            buttonContainerStyle={{
                backgroundColor: 'transparent'
            }}
            buttonStyle={{
                backgroundColor: 'transparent',
                width: BUTTON_SIZE
            }}
            selectedButtonStyle={{
                backgroundColor: 'transparent'
            }}
            innerBorderStyle={{
                color: 'transparent'
            }}
            selectedTextStyle={{
                color: 'transparent'
            }}
        />
    );
};

export default AudioPanel;
